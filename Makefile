# Makefile for MPICPL
# Author(s): aurelien.esnard@labri.fr

RELEASE= `cat RELEASE`
MPICPL_DIR=.
MAKEFILE_INC= $(MPICPL_DIR)/Makefile.inc
include $(MAKEFILE_INC)

all: 
	make -C test
	make -C src
	make -C tools
	make -C examples
	make -C examples/fortran

### install / uninstall
install:
	$(MKDIR) bin include lib
	$(CP) mpicpl.dtd tools/mpicplrun bin/
	$(CP) src/libmpicpl.a lib/
	$(CP) src/mpicplf.h src/mpicpl.h src/struct.h include/

uninstall:
	$(RM) *.tgz bin/ lib/ include/

### doxygen
dox:
	make -C doc

export-dox: 
	scp -r doc/html esnard@scm.gforge.inria.fr:/home/groups/mpicpl/htdocs/doc/mpicpl-$(RELEASE)

### useful for developer only
release:
	@echo "Exporting the last release of MPICPL from SVN (release $(RELEASE))"
	svn export svn+ssh://esnard@scm.gforge.inria.fr/svn/mpicpl/trunk/mpicpl
	@echo "Generating MPICPL archive mpicpl-$(RELEASE).tgz"
	tar cvzf mpicpl-$(RELEASE).tgz mpicpl
	@echo "Cleaning directoty"
	rm -rf mpicpl
	@echo "Done!"

### useful for developer only
tags:
	@echo "Edit the RELEASE file and type the following command..."
	@echo "svn copy svn+ssh://esnard@scm.gforge.inria.fr/svn/mpicpl/trunk/mpicpl svn+ssh://esnard@scm.gforge.inria.fr/svn/mpicpl/tags/mpicpl-$(RELEASE) -m \"Release $(RELEASE)\""

### clean
clean:
	make -C test clean
	make -C src clean
	make -C tools clean
	make -C examples clean
	make -C examples/fortran clean
	make -C doc clean
	$(RM) *.tgz bin/ lib/ include/