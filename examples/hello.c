/* MPI Coupling Library (Hello Example)      */
/* Filename: hello.c                         */
/* Author(s): A. Esnard                      */

#include <mpi.h>
#include "mpicpl.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>

/* ***************************************** */

int main( int argc, char *argv[] )
{  
  int prank, psize, univprank, univpsize;;
  MPICPL cpl;
  MPI_Comm parent;
  MPI_Comm universe;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_get_parent(&parent);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  univprank = prank;
  univpsize = psize;  
  char * code = "hello";

  if(argc == 2) code = argv[1]; 
  
  printf("[%s:%d] HELLO START\n", code, prank);
  
  /* coupling section */
  if (parent != MPI_COMM_NULL) {
    printf("[%s:%d] HELLO INIT...\n", code, prank);
    MPICPL_Init(&cpl, MPI_COMM_WORLD, code);
    printf("[%s:%d] HELLO INIT DONE\n", code, prank);
    printf("[%s:%d] HELLO RUN...\n", code, prank);
    MPICPL_Run(cpl); 
    printf("[%s:%d] HELLO RUN DONE\n", code, prank);

    // sleep 1s
    sleep(1);
    
    MPICPL_Get_univcomm(cpl, &universe); 
    MPI_Comm_rank(universe, &univprank);
    MPI_Comm_size(universe, &univpsize);
    
    /* send a test message */
    if(strcmp(code,"A") == 0)
      MPICPL_Message(cpl,"SOS TO THE WORLD!");

    MPICPL_Barrier(cpl);
  }
  
  printf("[%s:%d] HELLO WORLD (rank %d in universe of size %d) \n",  
   	 code, prank, univprank, univpsize);   
  
  if (parent != MPI_COMM_NULL) {    
    MPICPL_Finalize(cpl);
  }
  
  printf("[%s:%d] STOP\n", code, prank);
  fflush(stdout);  
  MPI_Finalize();
  return 0;
}

/* ***************************************** */
