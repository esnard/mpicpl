!     test
      
      program main
      
      implicit none

      include 'mpif.h'          
      include 'mpicplf.h'          

      integer prank, psize
      integer*4 ierr 
      integer*4 cpl 
      integer parentcomm
      
      call MPI_INIT(ierr)
      call MPI_COMM_GET_PARENT(parentcomm, ierr)
      call MPI_COMM_RANK(MPI_COMM_WORLD, prank, ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD, psize, ierr)

      if(parentcomm .ne. MPI_COMM_NULL) then
        call MPICPL_INIT(cpl, MPI_COMM_WORLD, "HELLO"//CHAR(0), ierr)
        call MPICPL_RUN(cpl, ierr)      
        call MPICPL_BARRIER(cpl, ierr)
      endif

      print *, "Hello World from ", prank, " of ", psize           

      if(parentcomm .ne. MPI_COMM_NULL) then
        call MPICPL_FINALIZE(cpl, ierr)       
      endif

      call MPI_FINALIZE(ierr)

!     print *,"Exit Fortran Program"

      end program main
