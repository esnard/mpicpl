/* MPI Coupling Library (Example of Code)    */
/* Filename: code.c                          */
/* Author(s): A. Esnard                      */

#include "mpi.h"
#include "mpicpl.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>

#define RESTART 0    /* 1 for yes ; 0 for no restart */

/* ***************************************** */

void demo(MPICPL cpl, int prank, int psize, char* code, int i)
{
  MPI_Comm intercommAB, intercommAC, intercommBC;
  int token = -1;
  int root;

  /* synchronize all codes */
  MPICPL_Barrier(cpl);
  
  /* get intercomm */
  intercommAB = intercommAC = intercommBC = MPI_COMM_NULL; 
  
  if(strcmp(code,"A") == 0 || strcmp(code,"B") == 0) {
    MPICPL_Get_intercomm(cpl,"AB",&intercommAB); 
  }
  if(strcmp(code,"A") == 0 || strcmp(code,"C") == 0) {
    MPICPL_Get_intercomm(cpl,"AC",&intercommAC); 
  }
  if(strcmp(code,"B") == 0 || strcmp(code,"C") == 0) {
    MPICPL_Get_intercomm(cpl,"BC",&intercommBC); 
  }
  
  /* token ring: A -> B -> C -> A */
  
  if(strcmp(code,"A") == 0) {
    token = i;
    printf("[%s:%d] TOKEN %d\n", code, prank+1, token);
    root = (prank == 0)?MPI_ROOT:MPI_PROC_NULL;
    MPI_Bcast(&token, 1, MPI_INT, root, intercommAB); // send to B
    root = 0;
    MPI_Bcast(&token, 1, MPI_INT, root, intercommAC); // recv from C
    printf("[%s:%d] TOKEN %d\n", code, prank+1, token);
  }
  else if(strcmp(code,"B") == 0) {
    root = 0;
    MPI_Bcast(&token, 1, MPI_INT, root, intercommAB); // recv from A
    root = (prank == 0)?MPI_ROOT:MPI_PROC_NULL;
    MPI_Bcast(&token, 1, MPI_INT, root, intercommBC); // send to C
    printf("[%s:%d] TOKEN %d\n", code, prank+1, token);
  }
  else if(strcmp(code,"C") == 0) {
    root = 0;      
    MPI_Bcast(&token, 1, MPI_INT, root, intercommBC); // recv from B
    root = (prank == 0)?MPI_ROOT:MPI_PROC_NULL;
    MPI_Bcast(&token, 1, MPI_INT, root, intercommAC); // send to A
    printf("[%s:%d] TOKEN %d\n", code, prank+1, token);
  }    
}

/* ***************************************** */

int main( int argc, char *argv[] )
{  
  int prank, psize;
  MPICPL cpl;
  MPI_Comm parentcomm;
  int restart = RESTART;  
  
  MPI_Init(&argc, &argv);
  MPI_Comm_get_parent(&parentcomm);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  assert(argc >= 2);
  char* code = argv[1];
  if(argc == 3) { 
    restart = 0;
    printf("[%s:%d] RESTART\n", code, prank+1);
  }
  else
    printf("[%s:%d] START\n", code, prank+1);
  
  /* coupling section */
  if (parentcomm != MPI_COMM_NULL) {
    MPICPL_Init(&cpl, MPI_COMM_WORLD, code);
    MPICPL_Run(cpl); 
    printf("[%s:%d] RUN\n", code, prank+1);
    
    /* demo of code coupling */
    for(int i = 0 ; i < 10 ; i++)
      demo(cpl, prank, psize, code, i);
    
    /* restart */
    if(restart == 1)  {
      char * new_program = "./code";
      int new_np = 2;
      char * new_argv[] = {code, "restart", NULL};
      char new_cwd[255];      
      assert(getcwd(new_cwd,255) != NULL);
      MPICPL_Restart(cpl, new_program, new_cwd, new_np, new_argv);     
    }
    
    MPICPL_Finalize(cpl);
  }
  
  printf("[%s:%d] STOP\n", code, prank+1);
  fflush(stdout);  
  MPI_Finalize();
  return 0;
}

/* ***************************************** */
