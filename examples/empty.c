/* MPI Coupling Library (Example of Code)    */
/* Filename: code.c                          */
/* Author(s): A. Esnard                      */

#include "mpi.h"
#include "mpicpl.h"
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

/* ***************************************** */

int main( int argc, char *argv[] )
{  
  MPICPL cpl;
  MPI_Comm parentcomm;
  MPI_Init(&argc, &argv);
  MPI_Comm_get_parent(&parentcomm);
  assert(parentcomm != MPI_COMM_NULL);
  if(argc == 2) {
    char* code = argv[1];  
    MPICPL_Init(&cpl, MPI_COMM_WORLD, code);
    fflush(stdout);
    MPICPL_Run(cpl); 
    MPICPL_Finalize(cpl);
  }
  sleep(1);
  MPI_Finalize();
  return 0;
}

/* ***************************************** */
