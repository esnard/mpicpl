/* MPI2 Spawn Benchmark                      */
/* Filename: bench-spawn.c                   */
/* Author: aurelien.esnard@labri.fr          */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* ***************************************** */

int main( int argc, char *argv[] )
{
  double starttime, endtime;
  
  if(argc != 3) {
    printf("Usage: %s nbcodes np\n", argv[0]);
    return EXIT_FAILURE;
  }
  
  int nbcodes = atoi(argv[1]);
  int np = atoi(argv[2]);
  
  MPI_Init(&argc, &argv);  
  starttime = MPI_Wtime();

  /* spawn codes */
  for(int i = 0; i < nbcodes ; i++) {  

  int err;    
  int errcodes[np];
  MPI_Comm bootcomm;
  
  err = MPI_Comm_spawn("./empty", 
		       MPI_ARGV_NULL,
		       np, 
		       MPI_INFO_NULL,
		       0, 
		       MPI_COMM_WORLD,
		       &bootcomm, 
		       errcodes); /* MPI_ERRCODES_IGNORE */

  assert(err == MPI_SUCCESS);        
  }
  
  /* This call will return when all spawn codes reach MPI_Init, and
     after the universe communicator has been created. */  
  
  endtime = MPI_Wtime();
  

  int prank;
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  if(prank == 0) {
    printf("%d %d %f\n", nbcodes, np, endtime-starttime); /* time in seconds */
    fflush(stdout);
  }
  
  MPI_Finalize();
  return 0;
}

/* ***************************************** */
