/* MPI Coupling: Benchmark                   */
/* Filename: bench.c                         */
/* Author: aurelien.esnard@labri.fr          */

#include "mpi.h"
#include "mpicplb.h"
#include <stdio.h>
#include <stdlib.h>

int prank, psize;
MPICPL cpl;

/* ***************************************** */

int main( int argc, char *argv[] )
{
  double starttime, endtime;
  
  if(argc != 3) {
    printf("Usage: %s nbcodes np\n", argv[0]);
    return EXIT_FAILURE;
  }

  int nbcodes = atoi(argv[1]);
  int np = atoi(argv[2]);
  const char * boot = "boot";

  /* init */
  MPI_Init(&argc, &argv);
  starttime = MPI_Wtime();
  MPICPL_Init_boot(&cpl, MPI_COMM_WORLD, boot, 0, MPICPL_DEBUG_NO);

  /* spawn multiple codes */
  for(int i = 0 ; i < nbcodes ; i++) {
    char id[255];
    sprintf(id,"%d",i);
    char * code = "./empty";
    char * args = id;
    MPICPL_Add_code(cpl, id, code, NULL, np, args);  
  }

  MPICPL_Run_boot(cpl); 

  /* This call will return when all spawn codes reach MPI_Init, and
     after the universe communicator has been created. */

  endtime   = MPI_Wtime();
  if(prank == 0) {
    printf("%d %d %f\n", nbcodes, np, endtime-starttime); /* time in seconds */
    fflush(stdout);
  }

  /* main */
  int req;
  char msg[MPICPL_MAX_MSG_SIZE];
  
  do {
    MPICPL_Wait(cpl, &req, msg);  
  }
  while(req != MPICPL_REQUEST_END);
  
  /* end */
  MPICPL_Finalize_boot(cpl);
  MPI_Finalize();
  return 0;
}



  





/* ***************************************** */
