/* MPI Coupling Library (Dynamic Client Example) */
/* Filename: server.c                            */
/* Author(s): A. Esnard                          */

#include <mpi.h>
#include "mpicpl.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

/* ***************************************** */

int main( int argc, char *argv[] )
{  
  int prank, psize, univprank, univpsize;;
  MPICPL cpl;
  MPI_Comm universe;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  
  if(argc != 2) {
    printf("usage: %s %s\n", argv[0], "bootport");
    exit(EXIT_FAILURE);  
  }
  int bootport = atoi(argv[1]);

  char * code = "SERVER";
  printf("[%s:%d] START\n", code, prank);
  MPICPL_Init(&cpl, MPI_COMM_WORLD, code);
  MPICPL_Run(cpl); 

  printf("[%s:%d] ACCEPTING...\n", code, prank); 
  MPI_Comm intercomm;
  MPICPL_Accept(cpl, bootport, &intercomm);
  printf("[%s:%d] ACCEPT DONE!\n", code, prank);  

  MPI_Barrier(intercomm); // synchro with client code

  printf("[%s:%d] HELLO WORLD\n", code, prank);  
  
  MPI_Comm_disconnect(&intercomm);
  MPICPL_Finalize(cpl);
  
  printf("[%s:%d] STOP\n", code, prank);
  fflush(stdout);  
  MPI_Finalize();
  return 0;
}

/* ***************************************** */
