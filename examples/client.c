/* MPI Coupling Library (Dynamic Client Example) */
/* Filename: client.c                             */
/* Author(s): A. Esnard                          */

#include <mpi.h>
#include "mpicpl.h"
#include "mpicplb.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

/* ***************************************** */

int main( int argc, char *argv[] )
{  
  int prank, psize;
  MPICPL cpl;
  MPI_Comm parent;
  MPI_Comm universe;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);

  char * code = "CLIENT";
  
  if(argc != 3) {
    printf("usage: %s %s\n", argv[0], "boothostname bootport");
    exit(EXIT_FAILURE);  
  }
  
  char * boothostname = argv[1];
  int bootport = atoi(argv[2]);
  
  printf("[%s:%d] START\n", code, prank);

  printf("[%s:%d] CONNECTING...\n", code, prank);
  MPI_Comm intercomm;
  MPICPL_Connect(MPI_COMM_WORLD, boothostname, bootport, &intercomm);
  printf("[%s:%d] CONNECT DONE!\n", code, prank);
  
  MPI_Barrier(intercomm); // synchro with server code
  
  printf("[%s:%d] HELLO WORLD\n", code, prank);  

  MPI_Comm_disconnect(&intercomm);  
  printf("[%s:%d] STOP\n", code, prank);
  fflush(stdout);  
  MPI_Finalize();
  return 0;
}

/* ***************************************** */
