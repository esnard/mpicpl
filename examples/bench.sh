#!/bin/bash

MAX=$1    # MAX NB CODES
NP=$2      # NB of PROC per CODE

echo "# NB_CODES NB_PROC_PER_CODE TIME(s)"
for N in `seq $MAX`
do
#    mpirun -np 1 -machinefile $HOME/machines ./bench-spawn $N $NP
#    mpirun -np 1 -machinefile $HOME/machines ./bench $N $NP
#    mpiexec -machinefile ~/machines -np 1 ./bench-spawn $N $NP
    mpiexec -machinefile ~/machines -np 1 ./bench $N $NP
done
