/* MPI Coupling: Example of Boot Code        */
/* Filename: boot.c                          */
/* Author: aurelien.esnard@labri.fr          */

#include "mpi.h"
#include "mpicplb.h"
#include <stdio.h>

int prank, psize;
MPICPL cpl;

/*                            */
/*             A              */
/*            / \             */
/*           B---C            */
/*                            */

int npA = 1;
int npB = 2;
int npC = 3;
// const char * argvA[] = {"./code", "A", NULL};
// const char * argvB[] = {"./code", "B", NULL};
// const char * argvC[] = {"./code", "C", NULL};
const char * A = "A";
const char * B = "B";
const char * C = "C";
const char * AB = "AB";
const char * AC = "AC";
const char * BC = "BC";
const char * code = "./code";

int MPICPL_Load_demo(MPICPL cpl)
{  
  // MPICPL_Add_code(cpl, A, code, NULL, npA, (char**)argvA+1);
  // MPICPL_Add_code(cpl, B, code, NULL, npB, (char**)argvB+1);
  // MPICPL_Add_code(cpl, C, code, NULL, npC, (char**)argvC+1);
  MPICPL_Add_code(cpl, A, code, NULL, npA, A);
  MPICPL_Add_code(cpl, B, code, NULL, npB, B);
  MPICPL_Add_code(cpl, C, code, NULL, npC, C);
  MPICPL_Add_binding(cpl, AB, A, B);
  MPICPL_Add_binding(cpl, AC, A, C); 
  MPICPL_Add_binding(cpl, BC, B, C); 
  
  return 0;
}

/* ***************************************** */

int main( int argc, char *argv[] )
{
  int req;
  const char * boot = "boot";
  MPI_Init(&argc, &argv);
  MPICPL_Init_boot(&cpl, MPI_COMM_WORLD, boot, 0, MPICPL_DEBUG_HIGH);
  MPICPL_Load_demo(cpl); 

  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  printf("[%s:%d] START\n", boot, prank+1);
  
  
  if(prank == 0) MPICPL_Print(cpl);
  
  MPICPL_Run_boot(cpl); 

  char msg[MPICPL_MAX_MSG_SIZE];
  
  do {
    MPICPL_Wait(cpl, &req, msg);  
    if(req ==  MPICPL_REQUEST_SYNCHRONIZE) {
      printf("[%s:%d] SYNCHRO\n", boot, prank+1);
    }
    else if(req ==  MPICPL_REQUEST_MESSAGE) {
      printf("[%s:%d] MESSAGE %s\n", boot, prank+1, msg);
    }
  }
  while(req != MPICPL_REQUEST_END);
  
  MPICPL_Finalize_boot(cpl);
  printf("[%s:%d] STOP\n", boot, prank+1);
  fflush(stdout);
  MPI_Finalize();
  return 0;
}

/* ***************************************** */
