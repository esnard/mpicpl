# Makefile for MPI Coupling (MPICPL)
# Author(s): aurelien.esnard@labri.fr

# Remark: The variable MPICPL_DIR must be defined before to include
# this file in Makefile.

### COMMON VARIABLES ###

RM:= rm -rf
MKDIR:= mkdir -p
CP:= cp -f

CC:= mpicc
FC:= mpif90
MPIRUN:= mpirun
MPIROOT:= /usr

# USER-DEFINED MPIROOT
# MPIROOT:= /opt/mpich2-1.4.1p1/install
# MPIROOT:= /opt/openmpi-1.5.4/install
# MPIROOT:= /opt/openmpi-1.5.3/install
# MPIROOT:= /opt/openmpi-trunk/install
# MPIROOT:=/opt/intel/softs/openmpi/1.5.4

CC:= $(MPIROOT)/bin/mpicc
FC:= $(MPIROOT)/bin/mpif90
MPIRUN:= $(MPIROOT)/bin/mpirun

XML_CFLAGS:= `xml2-config --cflags`
XML_LDFLAGS:= `xml2-config --libs` ### bug!

CFLAGS:= -std=c99 -g -pg # -fPIC
FFLAGS:= 
LDFLAGS:= 

MPICPL_CFLAGS:= -I$(MPICPL_DIR)/src -DDEBUG $(XML_CFLAGS) 
MPICPL_FFLAGS:= -I$(MPICPL_DIR)/src -DDEBUG
MPICPL_LDFLAGS:= -L$(MPICPL_DIR)/src -lmpicpl $(XML_LDFLAGS) -lm 

#LDFLAGS:= $(LDFLAGS) -Wl,-rpath,$(MPIROOT)/lib


### For Intel
# CC:= mpicc # -cc=icc
# FC:= mpif90
# FC:= mpif77 -fc=ifort
# LDFLAGS:= ${LDFLAGS} -lirc

### For Intel Trace Analyzer
# LDFLAGS:= -L $(VT_SLIB_DIR)  $(VT_ADD_LIBS) 
# CFLAGS:=  -g -trace


# EOF
