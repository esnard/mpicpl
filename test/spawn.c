/* MPI Coupling Library (Test MPI-2)         */
/* Filename: spawn.c                         */
/* Author(s): A. Esnard                      */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define NUM_SPAWNS 4

int main( int argc, char *argv[] )
{
  int world_rank, world_size, local_size, remote_size, merged_rank, merged_size;
  int np = NUM_SPAWNS;
  int errcodes[NUM_SPAWNS];
  MPI_Comm parentcomm, intercomm, intracomm;
  char * program = argv[0];
  
  MPI_Init( &argc, &argv );
  MPI_Comm_get_parent( &parentcomm );

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size); 

  /* master */
  if (parentcomm == MPI_COMM_NULL)
    {
      MPI_Comm_spawn(program, MPI_ARGV_NULL, np, 
		     MPI_INFO_NULL, 0, MPI_COMM_WORLD, /* root rank 0 */
		     &intercomm, errcodes );
      MPI_Intercomm_merge(intercomm, 0, &intracomm);	     
      MPI_Comm_size(intercomm, &local_size); 
      MPI_Comm_remote_size(intercomm, &remote_size); 
      MPI_Comm_rank(intracomm, &merged_rank);
      MPI_Comm_size(intracomm, &merged_size); 
      printf("I'm the master %d of %d, local size %d, remote size %d, rank %d of %d in universe.\n",
	     world_rank+1,world_size, local_size, remote_size, merged_rank+1, merged_size);
      
    }
  /* slave */
  else
    {
      MPI_Intercomm_merge(parentcomm, 1, &intracomm);		     
      MPI_Comm_size(parentcomm, &local_size); 
      MPI_Comm_remote_size(parentcomm, &remote_size);             
      MPI_Comm_rank(intracomm, &merged_rank);
      MPI_Comm_size(intracomm, &merged_size);       
      printf("I'm the slave %d of %d, local size %d, remote size %d, rank %d of %d in universe.\n",
	     world_rank+1,world_size, local_size, remote_size, merged_rank+1, merged_size);      
    }
    
  /* test intracomm here */
    
  fflush(stdout);
  MPI_Finalize();
  return 0;
}
