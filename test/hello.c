/* MPI Coupling Library (Test MPI-2)         */
/* Filename: hello.c                         */
/* Author(s): A. Esnard                      */

#include "mpi.h"
#include <stdio.h>

int prank, psize;
int hostnamelen;
char hostname[MPI_MAX_PROCESSOR_NAME];

int main( int argc, char *argv[] )
{
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  MPI_Get_processor_name(hostname, &hostnamelen);
  printf("Hello world from %s (rank %d of %d).\n", hostname, prank, psize);
  MPI_Finalize();
  return 0;
}
