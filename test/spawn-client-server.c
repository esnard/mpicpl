/* MPI Coupling Library (Test MPI-2)         */
/* Filename: spawn-client-server.c           */
/* Author(s): A. Esnard                      */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define TAG 777

int main( int argc, char *argv[] )
{
  int prank, psize;
  char port_name[MPI_MAX_PORT_NAME];     
  MPI_Comm parentcomm;
  
  MPI_Init( &argc, &argv );
  MPI_Comm_get_parent( &parentcomm );
  
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize); 
  
  /* master */
  if (parentcomm == MPI_COMM_NULL)
    {
      MPI_Comm sintercomm, cintercomm;
      printf("Master: %d of %d\n", prank, psize);
      
      int snp = 2, cnp = 2;
      int err;
      int errcodes[2];
      char * program = argv[0];
      char * cargv[] = {"0", NULL};
      char * sargv[] = {"1", NULL};
      
      /* spawn server */
      if(prank == 0) printf("Master spawn server code\n");
      err = MPI_Comm_spawn(program, sargv, snp, 
			   MPI_INFO_NULL, 0, MPI_COMM_WORLD, /* root rank 0 */
 			   &sintercomm, errcodes);
      
      assert(err == MPI_SUCCESS);
      
      /* spawn client */
      if(prank == 0) printf("Master spawn client code\n");
      err = MPI_Comm_spawn(program, cargv, cnp, 
			   MPI_INFO_NULL, 0, MPI_COMM_WORLD, /* root rank 0 */
			   &cintercomm, errcodes);

      
      assert(err == MPI_SUCCESS);
      
      if(prank == 0) {
	MPI_Status status;
	MPI_Recv(port_name, MPI_MAX_PORT_NAME, MPI_CHAR, 0, TAG, sintercomm, &status); 
	printf("Master recv from server port name: %s\n", port_name);
	MPI_Send(port_name, MPI_MAX_PORT_NAME, MPI_CHAR, 0, TAG, cintercomm); 
	printf("Master send to client port name: %s\n", port_name);
      }
      
      
    }
  /* slave */
  else
    {
      MPI_Comm intercomm; /* between client & server */
      assert(argc == 2);
      int isserver = atoi(argv[1]);
      
      /* server */
      if(isserver) {
	printf("Server: %d of %d\n", prank, psize);
	if (prank == 0) {
	  int err = MPI_Open_port(MPI_INFO_NULL, port_name);	  
	  if(err != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, err);
	  MPI_Send(port_name, MPI_MAX_PORT_NAME, MPI_CHAR, 0, TAG, parentcomm); 
	  printf("Server send port name: %s\n", port_name);
	}

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Comm_accept(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercomm);	
	if(prank == 0) {
	  printf("Server accept connection!\n");
	  MPI_Status status;
	  char message[255];
	  MPI_Recv(message, 255, MPI_CHAR, 0, TAG+1, intercomm, &status); 
	  printf("Server recv from client message: %s\n", message);
	}
      }
      /* client */
      else {
	printf("Client: %d of %d\n", prank, psize);
	
	if(prank == 0) {
	  MPI_Status status;
	  MPI_Recv(port_name, MPI_MAX_PORT_NAME, MPI_CHAR, 0, TAG, parentcomm, &status); 
	  printf("Client recv port name: %s\n", port_name);
	}

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Comm_connect(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercomm);
	if(prank == 0) { 
	  char message[255] = "hello world!";
	  printf("Client connected!\n");
	  MPI_Send(message, 255, MPI_CHAR, 0, TAG+1, intercomm); 
	  printf("Client send to server message: %s\n", message);
	}

      }

      MPI_Comm_disconnect(&intercomm);
    }
  
  /* test intracomm here */
  
  fflush(stdout);
  MPI_Finalize();
  return 0;
}
