/* MPI Coupling Library (Test MPI-2)         */
/* Filename: spawn-bridge.c                  */
/* Author(s): A. Esnard                      */

#include <mpi.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>

#define NPA 2
#define NPB 2
#define NPC 2
#define TAG 777

/* 
   - Doc : http://www.mpi-forum.org/docs/mpi-11-html/node113.html
   - Doc : http://www.mcs.anl.gov/research/projects/mpi/mpi-standard/mpi-report-2.0/node142.htm
   - Exemples faciles A-B-C : http://www.mpi-forum.org/docs/mpi-11-html/node114.html
   - Exemple de bridge AB-C : http://www.open-mpi.org/community/lists/users/att-16686/intercomm_create.c 
*/

/* 
   
   The current MPI interface provides only two intercommunicator
   construction routines: MPI_INTERCOMM_CREATE, creates an
   intercommunicator from two intracommunicators, MPI_COMM_DUP,
   duplicates an existing intercommunicator (or intracommunicator).
   The other communicator constructors, MPI_COMM_CREATE and
   MPI_COMM_SPLIT, currently apply only to intracommunicators.
   
   => http://www.mpi-forum.org/docs/mpi-20-html/node142.htm 

*/

/*

  => http://trac.mcs.anl.gov/projects/mpich2/browser/mpich2/trunk/src/mpi/comm/intercomm_create.c

	MPI_Intercomm_create - Creates an intercommuncator from two intracommunicators
182	
183	Input Parameters:
184	+ local_comm - Local (intra)communicator
185	. local_leader - Rank in local_comm of leader (often 0)
186	. peer_comm - Communicator used to communicate between a
187	              designated process in the other communicator. 
188	              Significant only at the process in 'local_comm' with
189	              rank 'local_leader'.
190	. remote_leader - Rank in peer_comm of remote leader (often 0)
191	- tag - Message tag to use in constructing intercommunicator; if multiple
192	  'MPI_Intercomm_creates' are being made, they should use different tags (more
193	  precisely, ensure that the local and remote leaders are using different
194	  tags for each 'MPI_intercomm_create').
195	
196	Output Parameter:
197	. comm_out - Created intercommunicator
198	
199	Notes:
200	   'peer_comm' is significant only for the process designated the
201	   'local_leader' in the 'local_comm'.
202	
203	  The MPI 1.1 Standard contains two mutually exclusive comments on the
204	  input intercommunicators.  One says that their repective groups must be
205	  disjoint; the other that the leaders can be the same process.  After
206	  some discussion by the MPI Forum, it has been decided that the groups must
207	  be disjoint.  Note that the `reason` given for this in the standard is
208	  `not` the reason for this choice; rather, the `other` operations on
209	  intercommunicators (like 'MPI_Intercomm_merge') do not make sense if the
210	  groups are not disjoint.
   
   
*/

int main( int argc, char *argv[] )
{
  int prank, psize;
  int isA, isB, isC;
  int err;
  
  MPI_Init(&argc, &argv);
  assert(argc == 2);
  int SPAWN = atoi(argv[1]); /* if 0, NO SPAWN, else SPAWN (>= 1) */
  
  MPI_Comm intracommA, intracommB, intracommC;
  MPI_Comm intracommAB, intracommAC;
  
  /* ********** SPAWN *********** */
  
  if(SPAWN >= 1) {
    
    MPI_Comm_rank(MPI_COMM_WORLD, &prank);
    MPI_Comm_size(MPI_COMM_WORLD, &psize); 
    
    MPI_Comm parentcomm;
    MPI_Comm_get_parent(&parentcomm);    
    isA = (parentcomm == MPI_COMM_NULL) && (argc == 2) && (atoi(argv[1]) == 1); 
    isB = (parentcomm != MPI_COMM_NULL) && (argc == 2) && (atoi(argv[1]) == 2);
    isC = (parentcomm != MPI_COMM_NULL) && (argc == 2) && (atoi(argv[1]) == 3);
    assert(isA || isB || isC);
    
    /* A (master) */
    if(isA) {
      /* intra A */
      err = MPI_Comm_dup(MPI_COMM_WORLD, &intracommA);
      assert(err == MPI_SUCCESS);
      
      MPI_Comm intercommAB, intercommAC;         
      int errcodes[2];
      char * program = argv[0]; /* this program itself !!! */
      char * argvB[] = {"2", NULL};
      char * argvC[] = {"3", NULL};      
      
      /* spawn B */
      if(prank == 0) printf("A: spawn B\n");
      err = MPI_Comm_spawn(program, argvB, NPB, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercommAB, errcodes);      
      assert(err == MPI_SUCCESS);
      
      /* spawn C */
      if(prank == 0) printf("A: spawn C\n");
      err = MPI_Comm_spawn(program, argvC, NPC, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercommAC, errcodes);            
      assert(err == MPI_SUCCESS);      
      
      /* merge AB */      
      err = MPI_Intercomm_merge(intercommAB, 0, &intracommAB); /* A < B */
      assert(err == MPI_SUCCESS);
      
      /* merge AC */      
      err = MPI_Intercomm_merge(intercommAC, 0, &intracommAC); /* A < C */
      assert(err == MPI_SUCCESS);      
      
      MPI_Comm_free(&intercommAB);
      MPI_Comm_free(&intercommAC);      
    }
    
    /* B (first slave) */
    if(isB)
      {      
	/* intra B */
	err = MPI_Comm_dup(MPI_COMM_WORLD, &intracommB);
	assert(err == MPI_SUCCESS);
	
	/* merge AB */
	MPI_Comm intercommAB;
	MPI_Comm_get_parent(&intercommAB);
	err = MPI_Intercomm_merge(intercommAB, 1, &intracommAB); /* A < B */
	assert(err == MPI_SUCCESS);
	
	MPI_Comm_free(&intercommAB);
      }
    
    /* C (second slave) */
    if(isC) {    
      /* intra C */
      err = MPI_Comm_dup(MPI_COMM_WORLD, &intracommC);
      assert(err == MPI_SUCCESS);      
      
      /* merge AC */
      MPI_Comm intercommAC;
      MPI_Comm_get_parent(&intercommAC);
      err = MPI_Intercomm_merge(intercommAC, 1, &intracommAC); /* A < C */
      assert(err == MPI_SUCCESS);    
      
      MPI_Comm_free(&intercommAC);
    }
    
  }
  
  /* ********** NO SPAWN (SPLIT) *********** */
  
  else {
    
    int gprank, gpsize; /* global rank */
    MPI_Comm_rank(MPI_COMM_WORLD, &gprank);
    MPI_Comm_size(MPI_COMM_WORLD, &gpsize); 
    printf("[WORLD:%d] size %d, pid %d\n", gprank, gpsize, getpid());
    
    assert(gpsize == NPA+NPB+NPC);
    isA = ((gprank >= 0) && (gprank < NPA)); 
    isB = ((gprank >= NPA) && (gprank < NPA+NPB)); 
    isC = ((gprank >= NPA+NPB) && (gprank < NPA+NPB+NPC)); 
    assert(isA || isB || isC);

    int color;
    if(isA) color = 0;
    if(isB) color = 1;
    if(isC) color = 2;
    
    /* create intracommA, B, C by splitting World */
    if(isA) err = MPI_Comm_split(MPI_COMM_WORLD, color, gprank, &intracommA);
    if(isB) err = MPI_Comm_split(MPI_COMM_WORLD, color, gprank, &intracommB);
    if(isC) err = MPI_Comm_split(MPI_COMM_WORLD, color, gprank, &intracommC);
    assert(err == MPI_SUCCESS);
    
    /* create intracommAB */
    err = MPI_Comm_split(MPI_COMM_WORLD, isA || isB, gprank, &intracommAB);
    assert(err == MPI_SUCCESS);
    if(isC) MPI_Comm_free(&intracommAB);
    
    /* create intracommAC */
    err = MPI_Comm_split(MPI_COMM_WORLD, isA || isC, gprank, &intracommAC);
    assert(err == MPI_SUCCESS);
    if(isB) MPI_Comm_free(&intracommAC);
    
    /* prank & psize for A, B, C */
    if(isA) MPI_Comm_rank(intracommA, &prank);
    if(isB) MPI_Comm_rank(intracommB, &prank);
    if(isC) MPI_Comm_rank(intracommC, &prank);
    if(isA) MPI_Comm_size(intracommA, &psize); 
    if(isB) MPI_Comm_size(intracommB, &psize);
    if(isC) MPI_Comm_size(intracommC, &psize);
    
  }    
  
  /* ********** TEST *********** */
  
  if(isA) printf("[A:%d] size %d, pid %d\n", prank, psize, getpid());
  if(isB) printf("[B:%d] size %d, pid %d\n", prank, psize, getpid());
  if(isC) printf("[C:%d] size %d, pid %d\n", prank, psize, getpid());
  
  if(isA) MPI_Barrier(intracommA);
  if(isB) MPI_Barrier(intracommB);
  if(isC) MPI_Barrier(intracommC);
  if(isA || isB) MPI_Barrier(intracommAB);
  if(isA || isC) MPI_Barrier(intracommAC);
  
  if(isA) assert(psize == NPA);
  if(isB) assert(psize == NPB);
  if(isC) assert(psize == NPC);
  
  int prankAB, psizeAB, prankAC, psizeAC;
  if(isA || isB) MPI_Comm_rank(intracommAB, &prankAB); 
  if(isA || isC) MPI_Comm_rank(intracommAC, &prankAC);  
  if(isA || isB) MPI_Comm_size(intracommAB, &psizeAB); 
  if(isA || isC) MPI_Comm_size(intracommAC, &psizeAC);  
  if(isA || isB) assert(psizeAB == NPA+NPB);
  if(isA || isC) assert(psizeAC == NPA+NPC);
  
  /* check remote leader rank in peer comm AC */
  if(isC && prank == 0) assert(prankAC == NPA); /* C0 */
  if(isA && prank == 0) assert(prankAC == 0); /* A0 */
  
  /* ********** CREATE INTERCOMM AB-C *********** */
  
  /* create intercomm AB-C */
  MPI_Comm intercommABC;
  
  /* A (master) */
  if(isA) 
    {    
      /* create intercomm AB-C */
      printf("[A:%d] MPI_Intercomm_create(intracommAB,0,intracommAC,1,TAG,&intercommABC)\n", prank);
      err = MPI_Intercomm_create(intracommAB,      /* MPI_Comm local_comm -> AB */ 
				 0,                /* int local_leader -> A0 */
				 intracommAC,      /* MPI_Comm peer_comm -> AC */
				 NPA,              /* int remote_leader -> C0 */
				 TAG,              /* int tag */
				 &intercommABC);   /* MPI_Comm *newintercomm */
      
    }
  
  /* B (first slave) */
  if(isB)
    {
      
      /* create intercomm AB-C */
      printf("[B:%d] MPI_Intercomm_create(intracommAB,0,MPI_COMM_NULL,0,TAG,&intercommABC)\n",prank);
      err = MPI_Intercomm_create(intracommAB,      /* MPI_Comm local_comm -> AB */ 
				 0,                /* int local_leader -> A0 */
				 MPI_COMM_NULL,    /* MPI_Comm peer_comm -> UNUSED ! */ 
				 0,                /* int remote_leader -> UNUSED ! */
				 TAG,              /* int tag */
				 &intercommABC);   /* MPI_Comm *newintercomm */     
    }
  
  /* C (second slave) */
  if(isC) {
    
    /* create intercomm AB-C */
    printf("[C:%d] MPI_Intercomm_create(intracommC,0,intracommAC,0,TAG,&intercommABC)\n",prank);
    err = MPI_Intercomm_create(intracommC,       /* MPI_Comm local_comm -> C */ 
			       0,                /* int local_leader -> C0 */
			       intracommAC,      /* MPI_Comm peer_comm -> AC */
			       0,                /* int remote_leader -> A0 */
			       TAG,              /* int tag */
			       &intercommABC);   /* MPI_Comm *newintercomm */   
  }
  
  assert(err == MPI_SUCCESS);
  
  /* ********** MERGE INTRACOMM ABC *********** */
  
  MPI_Comm intracommABC;
  
  if(isA || isB) {
    err = MPI_Intercomm_merge(intercommABC,0, &intracommABC); /* AB < C */
    assert(err == MPI_SUCCESS);
  }
  if(isC) {
    err = MPI_Intercomm_merge(intercommABC, 1, &intracommABC); /* AB < C */
    assert(err == MPI_SUCCESS);
  }
  
  /* test ABC */
  int prankABC, psizeABC;
  MPI_Comm_rank(intracommABC, &prankABC);
  MPI_Comm_size(intracommABC, &psizeABC);
  printf("[ABC:%d] inter-barrier AB-C (size %d)\n", prankABC, psizeABC);  
  MPI_Barrier(intercommABC); /* test */
  
  printf("[ABC:%d] intra-barrier ABC (size %d)\n", prankABC, psizeABC);  
  MPI_Barrier(intracommABC); /* test */
  
  if(!SPAWN) {
    MPI_Comm_compare(intracommABC, MPI_COMM_WORLD, &err);
    assert(err == MPI_CONGRUENT);
  }
  
  /* ********** CREATE INTERCOMM B-C (with MPI_Intercomm_create) *********** */
  
  /* create intercomm B-C & merge BC */
  MPI_Comm intercommBC, intracommBC;
  
  if(isB) {
    err = MPI_Intercomm_create(intracommB,       /* MPI_Comm local_comm -> AB */ 
			       0,                /* int local_leader -> B0 */
			       intracommABC,     /* MPI_Comm peer_comm -> ABC */
			       NPA+NPB,          /* int remote_leader -> C0 */
			       TAG+1,            /* int tag */
			       &intercommBC);    /* MPI_Comm *newintercomm */
    assert(err == MPI_SUCCESS); 
    err = MPI_Intercomm_merge(intercommBC, 0, &intracommBC); /* B < C */
    assert(err == MPI_SUCCESS);
    
  }
  if(isC) {
    err = MPI_Intercomm_create(intracommC,       /* MPI_Comm local_comm -> AB */ 
			       0,                /* int local_leader -> C0 */
			       intracommABC,     /* MPI_Comm peer_comm -> ABC */
			       NPA,              /* int remote_leader -> B0 */
			       TAG+1,            /* int tag */
			       &intercommBC);    /* MPI_Comm *newintercomm */
    assert(err == MPI_SUCCESS); 
    err = MPI_Intercomm_merge(intercommBC, 1, &intracommBC); /* B < C */
    assert(err == MPI_SUCCESS);
  }
  
  /* ********** CREATE INTERCOMM B-C (with MPI_Comm_create) *********** */
  
  /* => Warning: not sure/obvious MPI_Comm_create() works with inter-communicator !!! */

  /* MPI_Comm intercommBC; */
  
  /* MPI_Group group; */
  
  /* if(isA || isB) { */
  /*   MPI_Group groupAB; */
  /*   int psizeAB = 0; */
  /*   MPI_Comm_size(intercommABC, &psizeAB); */
  /*   assert(psizeAB == NPA + NPB); */
  /*   int ranks[NPB]; */
  /*   for(int i = 0 ; i < NPB ; i++) ranks[i] = NPA + i; */
  /*   MPI_Comm_group(intercommABC, &groupAB);  /\* group = AB *\/ */
  /*   MPI_Group_incl(groupAB, NPB, ranks, &group); /\* group = B *\/ */
  /*   MPI_Group_free(&groupAB); */
  /* }  */
  /* if(isC) { */
  /*   MPI_Comm_group(intercommABC, &group); /\* group = C *\/ */
  /* } */
  /* err = MPI_Comm_create(intercommABC, group, &intercommBC); */
  /* assert(err == MPI_SUCCESS); */
  /* if(isA) assert(intercommBC == MPI_COMM_NULL); */
  /* else assert(intercommBC != MPI_COMM_NULL); */
  /* MPI_Group_free(&group); */
  
  /* test BC */
  if(isB || isC) {
    int prankBC, psizeBC;
    MPI_Comm_rank(intracommBC, &prankBC);
    MPI_Comm_size(intracommBC, &psizeBC);
    printf("[BC:%d] inter-barrier B-C (size %d)\n", prankBC, psizeBC);
    MPI_Barrier(intercommBC); 
    printf("[BC:%d] intra-barrier B-C (size %d)\n", prankBC, psizeBC);
    MPI_Barrier(intracommBC);
  }
    
  /* ********** THE END! *********** */
  
  /* free */
  if(isA) MPI_Comm_free(&intracommA);  
  if(isB) MPI_Comm_free(&intracommB);  
  if(isC) MPI_Comm_free(&intracommC);  
  if(isA || isB) MPI_Comm_free(&intracommAB);
  if(isA || isC) MPI_Comm_free(&intracommAC);
  if(isB || isC) MPI_Comm_free(&intracommBC);
  if(isB || isC) MPI_Comm_free(&intercommBC);
  MPI_Comm_free(&intercommABC);
  MPI_Comm_free(&intracommABC);
  
  /* end */
  fflush(stdout);
  MPI_Finalize();
  return 0;
}
