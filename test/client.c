/* MPI Coupling Library (Test MPI-2)         */
/* Filename: client.c                        */
/* Author(s): A. Esnard                      */

#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <mpi.h>

int main(int argc, char **argv)
{
  MPI_Comm intercomm, mergedcomm;
  char port_name[MPI_MAX_PORT_NAME]; 
  char file_port_name[255] = ".portname";
  char *service_name = "test_server";
  int psize, prank, psize2, prank2;
  char message[256] = "";
  
  /* initialize MPI */
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);	
  printf("Client %d of %d: initializing MPI\n",prank,psize);  
  
  /* resolve server port from file */
  printf("Client %d of %d: looking for service...\n", prank, psize);      
  if (prank == 0) {
    int fd = open(file_port_name, O_RDONLY);
    read(fd, port_name, MPI_MAX_PORT_NAME);
    close(fd);
    printf("Client: read port name = %s\n", port_name);
  }
  
  /* establishes communication with server */
  printf("Client %d of %d: trying to connect server...\n", prank, psize);
  MPI_Comm_connect(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercomm);
  printf("Client %d of %d: OK communication established\n", prank, psize);
  
  /* merge communicator */
  MPI_Intercomm_merge(intercomm,0,&mergedcomm);
  MPI_Comm_size(mergedcomm, &psize2);	
  MPI_Comm_rank(mergedcomm, &prank2);
  printf("Client %d of %d: new rank %d of %d\n",prank, psize, prank2, psize2);      
  
  /* broadcast in merged intra-comm */
  if(prank2 == 0) strcpy(message,"coucou");
  MPI_Bcast(message, 50, MPI_CHAR, 0, mergedcomm);
  printf("Client %d of %d: broadcast message \"%s\"\n",prank,psize,message);
  
  /* disconnect from server */
  MPI_Comm_disconnect(&intercomm);
  printf("Client %d of %d: disconnection\n",prank,psize);
  
  /* finalize */
  MPI_Finalize();
  printf("Client %d of %d: finalize\n",prank,psize);
  
  return 0;
}       

/* eof */

