/* MPI Coupling Library (Test MPI-2)         */
/* Filename: test-thread.c                   */
/* Author(s): A. Esnard                      */

/* 
   
   NOTA BENE : L'entrelacement de plusieurs comm. collectives entre
   thread et processus ne marche pas et provoque un
   interblocage. Astuce : il faut utiliser des communicateurs
   diff�rents. En revanche, pas de pb pour des comm pt � pt avec des
   tags diff�rents, et m�me pour une comm collective mix� avec du
   pt-�-pt !
   
*/

#include <mpi.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>

MPI_Comm NEW_WORLD;      
char port_name[MPI_MAX_PORT_NAME];     
char *service_name = "test_server";
char message[256]; 
MPI_Status status;
int token = 0;

static void * thread_start(void *arg)
{
  printf("Thread start!\n");
  int prank, psize, flag;
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  printf("[thread] Hello world from %d on %d.\n", prank, psize);
  MPI_Is_thread_main(&flag);
  printf("[thread] Is main thread: %d\n", flag != 0);
  fflush(stdout);    
  
  
  /* test send/rcv mixed with barrier */
  /* sleep(2); */
  /* if(prank == 0) {  */
  /*   token = 3;  */
  /*   MPI_Send(&token, 1, MPI_INTEGER, 1, 126, MPI_COMM_WORLD);  */
  /* }   */

  /* test two collective comm at same time with different communicators */
  // int x = 0;
  // MPI_Bcast(&x, 1, MPI_INT, 0, NEW_WORLD);
  
  
  /* test broadcast */
  if(prank == 0) {
    // MPI_Barrier(NEW_WORLD);
    MPI_Barrier(MPI_COMM_WORLD); 
    strcpy(message,"coucou");
    MPI_Bcast(message, 256, MPI_CHAR, 0, MPI_COMM_WORLD);
    printf("[thread:%d] broadcast message \"%s\"\n", prank, message);
    int x = prank; int res = 0;
    MPI_Allreduce(&x, &res, 1, MPI_INT, MPI_SUM, NEW_WORLD);
    printf("[thread:%d] sum = %d\n", prank, res);
  }
  
  /* test send/recv */
  /* if(prank == 0) {  */
  /*   token = -1;  */
  /*   MPI_Send(&token, 1, MPI_INTEGER, 1, 128, MPI_COMM_WORLD);  */
  /* } */
  /* if(prank == 1) {  */
  /*   MPI_Recv(&token, 1, MPI_INTEGER, 0, 127, MPI_COMM_WORLD, &status);  */
  /*   printf("[thread:%d] recv token %d\n", prank, token); */
  /* } */
  
  
  
  return NULL;
}

/******************************************************************************/

int main( int argc, char *argv[] )
{
  int provided, flag, claimed, required = MPI_THREAD_MULTIPLE;
  pthread_t thread;
  int prank, psize;
  
  /* test MPI thread multiple support */
  MPI_Init_thread(&argc, &argv, required, &provided);
  MPI_Comm_dup(MPI_COMM_WORLD, &NEW_WORLD);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  printf("Hello world from %d on %d.\n", prank, psize);
  
  assert(psize > 1);
  
  if(prank == 0)  {
    printf("MPI thread-multiple support: %d\n", provided == MPI_THREAD_MULTIPLE);
    assert(provided == MPI_THREAD_MULTIPLE);

    MPI_Query_thread(&claimed);
    printf("Query thread thread level: %d\n"
	   "Required thread level %d\n" 
	   "Init thread level povided: %d\n", 
	   claimed, required, provided);
  }
  
  pthread_create(&thread, NULL, &thread_start, NULL);
  
  MPI_Is_thread_main(&flag);
  printf("Is main thread ? %d\n", flag != 0);
  fflush(stdout);    
  
  /* test send/rcv mixed with barrier */
  
  /* if(prank == 1) { */
  /*   MPI_Recv(&token, 1, MPI_INTEGER, 0, 126, MPI_COMM_WORLD, &status);  */
  /*   printf("[main:%d] recv token %d\n", prank, token); */
  /*   MPI_Barrier(MPI_COMM_WORLD); */
  /* } */
  
  
  /* test barrier/broadcast */
  if(prank != 0) {
    // MPI_Barrier(NEW_WORLD);
    MPI_Barrier(MPI_COMM_WORLD); 
    MPI_Bcast(message, 256, MPI_CHAR, 0, MPI_COMM_WORLD);
    printf("[main:%d] broadcast message \"%s\"\n", prank, message);
    int x = prank; int res = 0;
    MPI_Allreduce(&x, &res, 1, MPI_INT, MPI_SUM, NEW_WORLD);
    printf("[main:%d] sum = %d\n", prank, res);
  }
  
  /* test send/recv */
  /* if(prank == 0) {  */
  /*   token = 1;  */
  /*   MPI_Send(&token, 1, MPI_INTEGER, 1, 127, MPI_COMM_WORLD);  */
  /* } */
  /* if(prank == 1) {  */
  /*   MPI_Recv(&token, 1, MPI_INTEGER, 0, 128, MPI_COMM_WORLD, &status);  */
  /*   printf("[main:%d] recv token %d\n", prank, token); */
  /* }   */
  
  /* test two barrier at same time with different communicators */
  // MPI_Barrier(MPI_COMM_WORLD);
  
  pthread_join(thread, NULL);
  MPI_Finalize();
  return 0;
}
