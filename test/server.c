/* MPI Coupling Library (Test MPI-2)         */
/* Filename: server.c                        */
/* Author(s): A. Esnard                      */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <mpi.h>

MPI_Comm intercomm, mergedcomm;      
char port_name[MPI_MAX_PORT_NAME];     
char file_port_name[255] = ".portname";
char *service_name = "test_server";
MPI_Status status;  
int psize, prank, psize2, prank2;
char message[50] ="";

int main(int argc, char **argv)
{
  
  /* initialize MPI */
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  printf("Server %d of %d: initializing MPI\n",prank,psize);  
  
  /* write server port in file  */
  if (prank == 0) {
    int err = MPI_Open_port(MPI_INFO_NULL, port_name);	  
    if(err != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, err);
    printf("Server: write port name = %s\n", port_name);
    int fd = open(file_port_name, O_WRONLY | O_CREAT, 0644);
    write(fd, port_name, MPI_MAX_PORT_NAME);
    close(fd);
  }
  
  /* wait connection */
  printf("Server %d of %d: accepting connections... \n",prank,psize);
  MPI_Comm_accept(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercomm);
  
  /* merge communicator */
  MPI_Intercomm_merge(intercomm,1,&mergedcomm);
  MPI_Comm_size(mergedcomm, &psize2);	
  MPI_Comm_rank(mergedcomm, &prank2);
  printf("Server %d of %d: new rank %d of %d\n",prank, psize, prank2, psize2);  
  
  /* broadcast in merged intra-comm */
  if(prank2 == 0) strcpy(message,"coucou");
  MPI_Bcast(message, 50, MPI_CHAR, 0, mergedcomm);
  printf("Server %d of %d: broadcast message \"%s\"\n",prank,psize,message);
  
  /* disconnection */
  MPI_Comm_disconnect(&intercomm);
  printf("Server %d of %d: disconnection\n",prank,psize);
  
  /* finalize */
  MPI_Finalize();
  printf("Server %d of %d: finalize\n",prank,psize);
  
  return 0;
}	

/* eof */
