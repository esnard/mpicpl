/* MPI Coupling Library (Test MPI-2)         */
/* Filename: thread-cs.c                     */
/* Author(s): A. Esnard                      */

#include <mpi.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#define TAG 777
pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
MPI_Comm intercomm; /* between client & server & server-thread */
char port_name[MPI_MAX_PORT_NAME];     
bool connected = false;

/* SERVER-THREAD */
static void * server_thread_start(void *arg)
{
  MPI_Comm parentcomm;
  int prank, psize;
  MPI_Comm THREAD_WORLD; 

  MPI_Comm_get_parent(&parentcomm);
  MPI_Comm_dup(MPI_COMM_WORLD, &THREAD_WORLD); // to prevent deadlock from server code !!!
  MPI_Comm_rank(THREAD_WORLD, &prank);
  MPI_Comm_size(THREAD_WORLD, &psize);
  printf("[server-thread:%d] hello\n", prank);
  
  if (prank == 0) {
    int err = MPI_Open_port(MPI_INFO_NULL, port_name);	  
    if(err != MPI_SUCCESS) MPI_Abort(THREAD_WORLD, err);
    MPI_Send(port_name, MPI_MAX_PORT_NAME, MPI_CHAR, 0, TAG, parentcomm); 
    printf("[server-thread:%d] send port name: %s\n", prank, port_name);
  }
  
  MPI_Barrier(THREAD_WORLD);
  int err = MPI_Comm_accept(port_name, MPI_INFO_NULL, 0, THREAD_WORLD, &intercomm);	
  assert(err == MPI_SUCCESS);
  printf("[server-thread:%d] connection accepted!\n", prank);
  printf("[server-thread:%d] send signal...\n", prank);  
  pthread_mutex_lock(&mut);
  connected = true;
  pthread_mutex_unlock(&mut);
  pthread_cond_signal(&cond); // wake up server when connection is accepted!
  printf("[server-thread:%d] end\n", prank);  
  return NULL;
}


int main( int argc, char *argv[] )
{
  int prank, psize;
  int provided, flag, claimed, required = MPI_THREAD_MULTIPLE;
  pthread_t thread;
  MPI_Comm parentcomm;
  
  MPI_Init_thread(&argc, &argv, required, &provided);
  MPI_Comm_get_parent(&parentcomm);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  assert(provided == MPI_THREAD_MULTIPLE);
  
  
  /* MASTER */
  if (parentcomm == MPI_COMM_NULL)
    {
      MPI_Comm sintercomm, cintercomm;
      printf("[master:%d] hello\n", prank);
      
      int snp = 2, cnp = 2;
      int err;
      int errcodes[2];
      char * program = argv[0];
      char * cargv[] = {"0", NULL};
      char * sargv[] = {"1", NULL};
      
      /* spawn server */
      if(prank == 0) printf("[master:%d] spawn server code\n", prank);
      err = MPI_Comm_spawn(program, sargv, snp, 
			   MPI_INFO_NULL, 0, MPI_COMM_WORLD, /* root rank 0 */
 			   &sintercomm, errcodes);
      
      assert(err == MPI_SUCCESS);
      
      /* spawn client */
      if(prank == 0) printf("[master:%d] spawn client code\n", prank);
      err = MPI_Comm_spawn(program, cargv, cnp, 
			   MPI_INFO_NULL, 0, MPI_COMM_WORLD, /* root rank 0 */
			   &cintercomm, errcodes);
      
      
      assert(err == MPI_SUCCESS);
      
      if(prank == 0) {
	MPI_Status status;
	MPI_Recv(port_name, MPI_MAX_PORT_NAME, MPI_CHAR, 0, TAG, sintercomm, &status); 
	printf("[master:%d] recv from server port name: %s\n", prank, port_name);
	MPI_Send(port_name, MPI_MAX_PORT_NAME, MPI_CHAR, 0, TAG, cintercomm); 
	printf("[master:%d] send to client port name: %s\n", prank, port_name);
      }
      
      printf("[master:%d] end\n", prank);	
    }
  /* SLAVES */
  else
    {
      assert(argc == 2);
      int isserver = atoi(argv[1]);
      
      /* SERVER */
      if(isserver) {
	printf("[server:%d] hello\n", prank);
	pthread_create(&thread, NULL, &server_thread_start, NULL);

	int it = 0;
	/* working loop */
	while(1) {
	  printf("[server:%d] step %d\n", prank,  it);	  	  
	  int x = prank; int res = 0;
	  MPI_Allreduce(&x, &res, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	  if(connected) break;
	  usleep(50000); // 50 ms
	  it++;
	}

	/*
	printf("[server:%d] wait connection...\n", prank);	  	  
	pthread_mutex_lock(&mut);	  
	if(!connected) pthread_cond_wait(&cond, &mut);
	pthread_mutex_unlock(&mut);
	*/

	if(prank == 0) {
	  printf("[server:%d] connected!\n", prank);
	  MPI_Status status;
	  char message[255];
	  MPI_Recv(message, 255, MPI_CHAR, 0, TAG+1, intercomm, &status); 
	  printf("[server:%d] recv from client message: %s\n", prank, message);
	}   
	

	printf("[server:%d] join server thread...\n", prank);
	pthread_join(thread, NULL);
	printf("[server:%d] end\n", prank);	
      }

      /* CLIENT */
      else {
	printf("[client:%d] hello\n", prank);
	
	if(prank == 0) {
	  MPI_Status status;

	  MPI_Recv(port_name, MPI_MAX_PORT_NAME, MPI_CHAR, 0, TAG, parentcomm, &status); 
	  printf("[client:%d] recv port name: %s\n", prank, port_name);
	}
	MPI_Barrier(MPI_COMM_WORLD);

	printf("[client:%d] sleep before connect....\n", prank);
	sleep(3);

	printf("[client:%d] start connection\n", prank);
	int err = MPI_Comm_connect(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercomm);
	assert(err == MPI_SUCCESS);
	printf("[client:%d] connected!\n", prank);

	if(prank == 0) { 
	  char message[255] = "hello world!";
	  printf("[client:%d] sending to server message: %s\n", prank, message);
	  MPI_Send(message, 255, MPI_CHAR, 0, TAG+1, intercomm); 
	  printf("[client:%d] send done!\n", prank);
	  printf("[client:%d] end\n", prank);	
	}
	
      }
      
      MPI_Comm_disconnect(&intercomm);
    }
  
  
  
  fflush(stdout);
  MPI_Finalize();
  return 0;
}


