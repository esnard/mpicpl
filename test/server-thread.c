/* MPI Coupling Library (Test MPI-2)         */
/* Filename: server-thread.c                 */
/* Author(s): A. Esnard                      */

#include <mpi.h>

#include <assert.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/******************************************************************************/

static void * thread_start(void *arg)
{
  int flag;
  MPI_Comm intercomm, mergedcomm;
  char port_name[MPI_MAX_PORT_NAME]; 
  char file_port_name[255] = ".portname";
  char *service_name = "test_server";
  int psize, prank, psize2, prank2;
  char message[256] = "";

  printf("Thread start!\n");

  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  printf("[thread] Hello world from %d on %d.\n", prank, psize);
  MPI_Is_thread_main(&flag);
  printf("[thread] Is main thread: %d\n", flag != 0);
  fflush(stdout);    

  MPI_Barrier(MPI_COMM_WORLD);

  /* write server port in file  */
  if (prank == 0) {
    int err = MPI_Open_port(MPI_INFO_NULL, port_name);	  
    if(err != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, err);
    printf("Server: port name = %s\n", port_name);
    int fd = open(file_port_name, O_WRONLY | O_CREAT, 0644);
    write(fd, port_name, MPI_MAX_PORT_NAME);
    close(fd);
  }
  
  /* wait connection */
  printf("[thread] Server %d of %d: accepting connection... \n", prank, psize); fflush(stdout);
  MPI_Comm_accept(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercomm);
  printf("[thread] Server %d of %d: connection accepted!\n", prank, psize);

  /* merge communicator */
  MPI_Intercomm_merge(intercomm,1,&mergedcomm);
  MPI_Comm_size(mergedcomm, &psize2);	
  MPI_Comm_rank(mergedcomm, &prank2);
  printf("[thread] Server %d of %d: new rank %d of %d\n",prank, psize, prank2, psize2);      

  /* broadcast in merged intra-comm */
  if(prank2 == 0) strcpy(message,"coucou");
  MPI_Bcast(message, 50, MPI_CHAR, 0, mergedcomm);
  printf("Server %d of %d: receive message \"%s\"\n",prank,psize,message);
  
  /* disconnection */
  MPI_Comm_disconnect(&intercomm);
  printf("[thread] Server %d of %d: disconnection\n",prank,psize);

  /* finalize */  
  printf("[thread] Server %d of %d: end of thread!!!\n",prank,psize);
  return NULL;
}

/******************************************************************************/

int main( int argc, char *argv[] )
{
  int provided, required = MPI_THREAD_MULTIPLE;
  pthread_t thread;
  int prank, psize;

  MPI_Init_thread(&argc, &argv, required, &provided);

  /* test MPI thread multiple support */
  printf("MPI thread-multiple support: %d\n", provided == MPI_THREAD_MULTIPLE );

  assert(provided == MPI_THREAD_MULTIPLE);
  
  /* start new thread */
  pthread_create(&thread, NULL, &thread_start, NULL);

  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  printf("[main] Hello world from %d on %d.\n", prank, psize);
  
  /* MPI_Comm_accept() must be reached before all... else it doesn't work!!! */
  sleep(1); 

  if(prank != 0) MPI_Barrier(MPI_COMM_WORLD);

  /* wait end of thread */
  pthread_join(thread, NULL);

  if(prank == 0) MPI_Barrier(MPI_COMM_WORLD);

  printf("[main] Finalize from %d on %d.\n", prank, psize);
  MPI_Finalize();
  return 0;
}
