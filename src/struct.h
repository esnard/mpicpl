/** 
 * @file  struct.h                       
 * @brief MPICPL Internal Structure
 * @author Aurelien Esnard                     
 */

#ifndef __STRUCT_H__
#define __STRUCT_H__

#include "mpi.h"

#define MPICPL_SUCCESS               MPI_SUCCESS
#define MPICPL_ERROR                 -1
#define MPICPL_BOOT_CODE             0
#define MPICPL_SPAWN_CODE            1 
#define MPICPL_CLIENT_CODE           2
#define MPICPL_TAG                   111
#define MPICPL_REQUEST_END           0
#define MPICPL_REQUEST_SYNCHRONIZE   1
#define MPICPL_REQUEST_RESTART       2
#define MPICPL_REQUEST_MESSAGE       3
#define MPICPL_REQUEST_LISTEN        4
#define MPICPL_MAX_MSG_SIZE          255
#define MPICPL_MAX_CHAR              255
#define MPICPL_PUBLISH_TAG           333
#define MPICPL_USLEEP_CONNECT        25*1000 /* 25 ms */

/* keep C++ compilers from getting confused. */
#if defined(__cplusplus)
extern "C" {
#endif
  
  /* ***************************************** */
  /*              MPICPL HANDLER               */
  /* ***************************************** */
  
  /** MPICPL handler */
  typedef struct MPICPL_t * MPICPL;
  
  
  /* ***************************************** */
  /*                 TYPES                     */
  /* ***************************************** */
  
  /** structure for code description */
  struct MPICPL_Code_t {    
    char id[MPICPL_MAX_CHAR];
    char program[MPICPL_MAX_CHAR];
    char cwd[MPICPL_MAX_CHAR];
    int np;
    char args[MPICPL_MAX_CHAR]; /* all args separated by a white space, without program name */
    /* misc. */
    MPI_Comm bootcomm;
    char univ_portname[MPI_MAX_PORT_NAME]; /* use to store intermediate port name when adding this code in universe */
  };
  
  typedef struct MPICPL_Code_t * MPICPL_Code;
  
  /* ***************************************** */
  
  /** structure for binding description */
  struct MPICPL_Binding_t {
    char id[MPICPL_MAX_CHAR];
    char client[MPICPL_MAX_CHAR];
    char server[MPICPL_MAX_CHAR];
    /* misc. */
    MPI_Comm intercomm;
    char portname[MPI_MAX_PORT_NAME]; 
  };
  
  typedef struct MPICPL_Binding_t * MPICPL_Binding;
  
  /* ***************************************** */
  
  /** MPICPL main universe structure */
  struct MPICPL_t {
    char id[MPICPL_MAX_CHAR];     /* code id */
    int kind;                     /* is boot/spawn or client code */
    MPI_Comm worldcomm;           /* world intra-communicator */
    /* desc. */
    int nb_codes;                 /* nb of codes */
    int nb_bindings;              /* nb of bindings */
    MPICPL_Code* codes;           /* array of codes (of size nb codes)*/
    MPICPL_Binding* bindings;     /* array of bindings (of size nb bindings) */
    /* misc. */
    MPI_Comm univcomm;            /* universe intra-communicator (without boot code) */
    MPI_Comm parentcomm;          /* inter-communicator with boot code (for spawn codes)*/
    int prank, psize;             /* prank, psize in worldcomm */
    int detached;                 /* detached mode */
    int debug;                    /* debug level (see mydebug.h) */
    // char boothostname[MPI_MAX_PROCESSOR_NAME]; /* socket hostname for dynamic connection management */
    // int bootport;                 /* socket port for dynamic connection management */
  };
  
  
#if defined(__cplusplus)
}
#endif

#endif /* __STRUCT_H__ */
