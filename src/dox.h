
/** \mainpage MPICPL User's Guide

MPICPL (MPI CouPLing) is a software library dedicated to the coupling
of parallel legacy codes, that are based on the wellknow MPI
standard. It proposes a lightweight and comprehensive programing
interface that simplifies the coupling of several MPI codes (2, 3 or
more). MPICPL facilitates the deployment of these codes thanks to the
mpicplrun tool and it interconnects them automatically through
standard MPI intercommunicators.  Moreover, it generates the universe
communicator, that merges the world communicators of all target
codes. The coupling infrastructure is described by a simple XML file,
that is just loaded by the mpicplrun tool.  

MPICPL was developed by the INRIA HiePACS project team for the purpose
of the ANR NOSSI project (http://nossi.gforge.inria.fr). It uses
advanced features of MPI2 standard. It is publicly available at INRIA
Gforge (http://mpicpl.gforge.inria.fr).

This manual is divided in the following sections:
- \ref intro "Introduction"
- \ref install "Install"
- \ref mpicplrun "The MPICPLRun tool"
- \ref mpicplapi "The MPICPL API"

// *****************************************************************
\section intro Introduction
// *****************************************************************

MPICPL provides a simple way to couple several MPI codes (2, 3 or
more). It facilitates the deployment of these codes and interconnect
them automatically in two different ways:
- two by two, by the use of MPI intercommunicator ; 
- all together, through the "universe" intracommunicator.

The framework of MPICPL distinguishes two kind of codes:
- <em>the boot-code</em>, that is a "special" program that spawns the
  other codes to be coupled and acts as the coordinator of the MPICPL
  framework (see \ref mpicplrun "MPICPLRun") ;
- <em>the spawn-code</em>, that is a "normal" end-user program spawned
  by the boot-code and that is intended to be coupled to other
  spawn-codes thanks to the MPICPL framework.

\image html mpicpl-overview.png "Overview"

Each spawn-code is indentified by a unique string ID (called "code
id") and starts with its own "world" communicator independent of other
spawn-codes (i.e. MPI_COMM_WORLD). One pair of coupled codes is
identified by a unique string ID (called "binding id") and they can
communicate thanks to an intercommunicator provided by the MPICPL
framework (each code defining a group within the intercommunicator).
Moreover, MPICPL provides the "universe" intracommunicator that merges
the "world" communicator of all spawn-codes.

// *****************************************************************
\section install Install
// *****************************************************************

First, decompress the MPICPL archive, that is available at INRIA
Gforge (http://mpicpl.gforge.inria.fr).

\verbatim
  $ tar xvzf mpicpl.tgz
\endverbatim

Look inside the file Makefile.inc. In particular, you can configure
these variables depending on your system:

- CC:= mpicc
- FC:= mpif90
- MPIRUN:= mpirun
- MPIROOT:= /usr

Then, compile and install. The MPICPL library and the MPICPLRun tool
are installed into the root directory of MPICPL, in sub-directories
<tt>bin</tt>, <tt>lib</tt> and <tt>include</tt>.

\verbatim
  $ make ; make install
\endverbatim

You can find in the "test" directory some MPI/MPI2 tests that are
useful to check if your MPI version fulfills all requirements for
MPICPL. The following command lists all available tests.

\verbatim
  $ cd test ; make test
\endverbatim

In the "examples" directory, you will find simple and more complex
examples of MPICPL. The following command lists all available examples.

\verbatim
  $ cd examples ; make test
\endverbatim

// *****************************************************************
\section mpicplrun The MPICPLRUN tool
// *****************************************************************

The most simple way to couple codes with MPICPL is to use the
MPICPLRun tool. This is a generic boot-code that loads an XML
description of the coupled application to deploy. In practice, you
will use <tt>mpicplrun</tt> instead of <tt>mpirun</tt> to start your
coupled application.

Usage:
\verbatim
  $ mpicplrun [-d] [-k] [-v dtdfile] xmlfile [xmlargs ...]
	 -v dtdfile: validate xmlfile against dtdfile
	 -k: detached mode (kill mpicplrun after spawning codes)
	 -d: debug mode
\endverbatim

Here is a sample XML description of three coupled codes (A, B and C)
that are interconnected two by two through bindings (AB, AC and BC).

\code 
<universe id="sample" args="npA,npB,npC">

  <!-- declare all codes -->
  <codes>
    <code id="A" program="./codeA" np="${npA}" args="..." />
    <code id="B" program="./codeB" np="${npB}" args="..." />
    <code id="C" program="./codeC" np="${npC}" args="..." />
  </codes>

  <!-- interconnect codes -->
  <bindings>
    <binding id="AB" client="A" server="B" />
    <binding id="AC" client="A" server="C" />
    <binding id="BC" client="B" server="C" />
  </bindings>

</universe>
\endcode

To launch this sample application, use the following command for
instance.

\verbatim
  $ mpirun -np 1 -machinefile hosts mpicplrun sample.xml 1 2 3
\endverbatim

In this example, the variables npA, npB and npB in the XML file are
substituted to 1, 2 and 3. Thus, the "universe" communicator of MPICPL
has a size of 6 MPI processes. The program {\tt mpicplrun} is executed
on the first host of the "machine file" and the end-user codes (codeA,
codeB and codeC) are spawn on the following hosts specified in the
"machine file".

For further details on XML description used by the MPICPLRun tool,
see the DTD file (mpicpl.dtd).

// *****************************************************************
\section mpicplapi The MPICPL API
// *****************************************************************

See detail of \ref mpicpl "MPICPL API".

// *****************************************************************
\section example Example
// *****************************************************************

Here is a simple template of a spawn-code within MPICPL. It
corresponds to the code with ID "A" in the previous XML description.

\code
#include <mpi.h>
#include "mpicpl.h"

int main( int argc, char *argv[] )
{  
  MPI_Comm parentcomm;
  MPI_Init(&argc, &argv);                     // 0) initialize MPI env.
  MPI_Comm_get_parent(&parentcomm);

  // coupled section
  if (parentcomm != MPI_COMM_NULL) {          // if this code is started by mpicplrun
    MPICPL cpl;
    MPICPL_Init(&cpl, MPI_COMM_WORLD, "A");   // 1) initialize MPICPL env.
    MPICPL_Run(cpl);                          // 2) run (interconnect spawn codes)
    // ...
    // ...
    // ...
    MPICPL_Finalize(cpl);                     // 3) finalize MPICPL env.
  }

  MPI_Finalize();                             // 4) finalize MPI env.
  return 0;
}
\endcode

Compile this code, assuming MPICPL_ROOT is set to the root directory of MPICPL.

\verbatim
  $ mpicc -std=c99 -I $(MPICPL_ROOT)/include/ -L $(MPICPL_ROOT)/lib/  codeA.c -o codeA -lmpicpl `xml2-config --libs` 
\endverbatim


*/

