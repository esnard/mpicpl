/* MPIPI library                             */
/* Filename: mpipi.c                         */
/* Author(s): A. Esnard                      */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define __USE_BSD
#include <unistd.h> /* usleep */

#include <mpi.h>
#include "mpipi.h"
#include "mydebug.h"

#define NO_MPI_PUBLISH

/* ***************************************** */
/*  Standard MPI publish/lookup mechanism    */
/* ***************************************** */
/*
  
  OPENMPI LOCAL/GLOBAL SCOPE
  
  Open MPI supports two name scopes: global and local. Local
  scope will place the specified service/port pair in a data
  store located on the mpirun of the calling process' job. Thus,
  data published with local scope will only be accessible to
  processes in jobs spawned by that mpirun - e.g., processes in
  the calling process' job, or in jobs spawned via
  MPI_Comm_spawn.
  
  Global scope places the specified service/port pair in a data
  store located on a central server that is accessible to all
  jobs running in the cluster or environment.  Thus, data
  published with global scope can be accessed by multiple mpiruns
  and used for MPI_Comm_Connect and MPI_Comm_accept between jobs.
  
  Note that global scope operations require both the presence of
  the central server and that the calling process be able to
  communicate to that server. MPI_Publish_name will return an
  error if global scope is specified and a global server is
  either not specified or cannot be found.
  
  Open MPI provides a server called ompi-server to support global
  scope operations. Please refer to its manual page for a more
  detailed description of data store/lookup operations.
  
*/

static 
int MPIPI_Publish_mpi2(MPI_Comm comm,
		       char * id,
		       char * service_name,
		       char * port_name,  /* out */
		       int debug)
{
  int prank = 0;
  int err;
  
  /* disable MPI fatal errors */
  MPI_Comm_set_errhandler(comm, MPI_ERRORS_RETURN);
  
  /* open port */
  err = MPI_Open_port(MPI_INFO_NULL, port_name); /* out */
  PRINT_HIGH(debug, id, prank, "OPEN PORT %s\n",port_name);
  assert(err == MPI_SUCCESS);
  
  /* prepare extra info */
  MPI_Info info = MPI_INFO_NULL;  
#ifdef USE_OPENMPI_GLOBAL_SCOPE
  MPI_Info_create(&info);
  MPI_Info_set(info, "ompi_global_scope", "true"); 
#else
  MPI_Info_create(&info);
  MPI_Info_set(info, "ompi_global_scope", "false"); 
#endif
  
  PRINT_HIGH(debug, id, prank, "PUBLISHING SERVICE %s\n", service_name);    
  err = MPI_Publish_name(service_name, info, port_name);
  
  /* MPI fatal errors (default) */
  MPI_Comm_set_errhandler(comm, MPI_ERRORS_ARE_FATAL);
  
  if(err != MPI_SUCCESS) {
    PRINT_ERROR(debug, id, prank, "ERROR: PUBLISHING SERVICE %s\n", service_name);
    MPI_Abort(comm, 1);
    return err;
  }
  
  PRINT_HIGH(debug, id, prank, "PUBLISHING SERVICE %s DONE!\n", service_name);        
  return err;
}

/* ***************************************** */

static 
int MPIPI_Lookup_mpi2(MPI_Comm comm,
		      char * id,
		      char * service_name,
		      char * port_name,  /* out */
		      int debug)
{
  int prank = 0;
  int err;
  
  PRINT_HIGH(debug, id, prank, "LOOKING FOR SERVICE %s\n", service_name);
  
  /* prepare extra info */  
  MPI_Info info = MPI_INFO_NULL;  
#ifdef USE_OPENMPI_GLOBAL_SCOPE
  MPI_Info_create(&info);
  MPI_Info_set(info, "ompi_global_scope", "true");
#else
  MPI_Info_create(&info);
  MPI_Info_set(info, "ompi_global_scope", "false"); 
#endif
  
  err = MPI_Lookup_name(service_name, info, port_name);
  PRINT_HIGH(debug, id, prank, "LOOKING FOR SERVICE %s DONE!\n", service_name);
  
  if(err != MPI_SUCCESS) {
    PRINT_WARNING(debug, id, prank, "WARNING: LOOKING FOR SERVICE %s FAILED\n", service_name);
  }
  
  return  err;  
}

/* ***************************************** */
/*  Internal MPIPI publish/lookup mechanism  */
/* ***************************************** */

static 
int MPIPI_Publish_file(MPI_Comm comm,
		       char * id,
		       char * service_name,
		       char * port_name, /* out */
		       int debug)
{
  int prank = 0;
  int err;
  
  /* open port */
  err = MPI_Open_port(MPI_INFO_NULL, port_name); /* out */
  PRINT_HIGH(debug, id, prank, "OPEN PORT %s FOR SERVICE %s\n", port_name, service_name);
  assert(err == MPI_SUCCESS);
  
  /* make $HOME/.mpicpl directory */
  char * home = getenv("HOME");
  assert(home != NULL);
  char dir_name[255];
  sprintf(dir_name,"%s/%s",home, ".mpicpl");
  mkdir(dir_name, 0755);
  
  /* write port_name in $HOME/.mpicpl/service_name.txt */
  char file_name[255];
  sprintf(file_name,"%s/%s.txt",dir_name, service_name);
  PRINT_HIGH(debug, id, prank, "PUBLISHING SERVICE %s IN FILE %s\n", service_name, file_name);
  int fd = open(file_name, O_WRONLY | O_CREAT | O_TRUNC, 0644);
  if(fd < 0) {
    PRINT_ERROR(debug, id, prank, "ERROR: PUBLISHING SERVICE %s\n", service_name);
    MPI_Abort(comm, 1);
  }
  write(fd, port_name, MPI_MAX_PORT_NAME);
  close(fd);
  
  PRINT_HIGH(debug, id, prank, "PUBLISHING SERVICE %s DONE!\n", service_name);        
  return MPI_SUCCESS;
}

/* ***************************************** */

static 
int MPIPI_Lookup_file(MPI_Comm comm,
		      char * id,
		      char * service_name,
		      char * port_name, /* out */
		      int debug)
{
  int prank = 0;
  int err;
  
  PRINT_HIGH(debug, id, prank, "LOOKING FOR SERVICE %s\n", service_name);
  
  /* make $HOME/.mpicpl directory */
  char * home = getenv("HOME");
  assert(home != NULL);
  char file_name[255];
  sprintf(file_name,"%s/%s/%s.txt",home, ".mpicpl", service_name);
  
  /* open file */
  int fd = open(file_name, O_RDONLY);
  if(fd < 0) {
    PRINT_ERROR(debug, id, prank, "ERROR: LOOKING FOR SERVICE %s IN FILE %s\n", 
		service_name, file_name);
    MPI_Abort(comm, 1);
  }
  read(fd, port_name, MPI_MAX_PORT_NAME);
  close(fd);
  PRINT_HIGH(debug, id, prank, "LOADING PORT %s FOR SERVICE %s IN FILE %s\n", 
	     port_name, service_name, file_name);  
  PRINT_HIGH(debug, id, prank, "LOOKING FOR SERVICE %s DONE!\n", service_name);
  
  return  MPI_SUCCESS;  
}

/* ***************************************** */
/*     MPIPI Connect & Accept Routine        */
/* ***************************************** */

int MPIPI_Connect_(MPI_Comm comm,
		   char * id,
		   char * service_name,
		   char * port_name, 
		   MPI_Comm * intercomm,
		   int debug)
{
  int prank;
  int root = 0;
  int err;
  MPI_Comm_rank(comm, &prank);  
  
  /* resolve server code from naming service */
  if (prank == root) {
    // err = MPIPI_Lookup_mpi2(comm, id, service_name, port_name, debug);
    err = MPIPI_Lookup_file(comm, id, service_name, port_name, debug);
    if(err != MPI_SUCCESS) return err;
  }
  
  /* establishes communication with server */
  PRINT_HIGH(debug, id, prank, "CONNECTING TO SERVICE %s...\n", service_name);
  err = MPI_Comm_connect(port_name,  /* used only on client root */
    			 MPI_INFO_NULL,
    			 root,
    			 comm,
    			 intercomm);
  
  if(err != MPI_SUCCESS) {
    PRINT_WARNING(debug, id, prank, "WARNING: FAIL TO CONNECT TO SERVICE %s\n", service_name);
    return err;
  }
  
  PRINT_LOW(debug, id, prank, "CONNECTION ESTABLISHED FOR SERVICE %s\n", service_name);  
  return MPI_SUCCESS;    
}

/* ***************************************** */

int MPIPI_Connect(MPI_Comm comm,
		  char * id,
		  char * service_name,
		  char * port_name, 
		  MPI_Comm * intercomm,
		  int debug)
{
  int prank;
  int err;
  int ntry = 1;
  MPI_Comm_rank(comm, &prank);
  
  /* disable MPI fatal errors */
  MPI_Comm_set_errhandler(comm, MPI_ERRORS_RETURN);
  
  do {
    PRINT_HIGH(debug, id, prank, "TRY %d TO CONNECT FOR SERVICE %s\n", ntry, service_name);
    /* try to connect... */
    err = MPIPI_Connect_(comm, id, service_name, port_name, intercomm, debug);
    usleep(MPIPI_USLEEP_CONNECT);
    ntry++;
  } while(err != MPI_SUCCESS && ntry < MPIPI_MAX_TRY_CONNECT);
  
  /* MPI fatal errors (default) */
  MPI_Comm_set_errhandler(comm, MPI_ERRORS_ARE_FATAL);
  
  /* check */
  if(ntry == MPIPI_MAX_TRY_CONNECT) {
    PRINT_ERROR(debug, id, prank, "FAILED TO CONNECT FOR SERVICE %s\n", service_name);
    MPI_Abort(comm, 1);
  } 
  
  return MPI_SUCCESS;
}

/* ***************************************** */

int MPIPI_Accept(MPI_Comm comm,	
		 char * id,
		 char * service_name,
		 char * port_name, 
		 MPI_Comm * intercomm,
		 int debug)
{
  int prank;
  int root = 0;
  int err;
  MPI_Comm_rank(comm, &prank);
  
  /* publish server in naming service */
  if (prank == root) {
    // err = MPIPI_Publish_mpi2(comm, id, service_name, port_name, debug);
    err = MPIPI_Publish_file(comm, id, service_name, port_name, debug);
    if(err != MPI_SUCCESS) return err;    
  }
  
  /* wait connection */
  PRINT_HIGH(debug, id, prank, "ACCEPTING CONNECTION FOR SERVICE %s...\n", service_name);
  err = MPI_Comm_accept(port_name, /* used only on server root */
  			MPI_INFO_NULL,
  			root,
  			comm,
  			intercomm); /* out */
  
  if(err != MPI_SUCCESS) {
    PRINT_ERROR(debug, id, prank, "ERROR: ACCEPTING CONNECTION FOR SERVICE %s...\n", service_name);
    MPI_Abort(comm, 1);
  }
  
  PRINT_LOW(debug, id, prank, "CONNECTION ESTABLISHED FOR SERVICE %s\n", service_name);
  
  return MPI_SUCCESS;      
}

/* ***************************************** */
