/** 
 * @file  mpicplb.h                       
 * @brief MPICPL API for Boot Code
 * @author Aurelien Esnard                     
 * @defgroup mpicplb API for Boot Code (mpicplb.h)
 */

#ifndef __MPICPLB_H__
#define __MPICPLB_H__

#include "mpi.h"
#include "struct.h"
#include "mydebug.h"

/* keep C++ compilers from getting confused. */
#if defined(__cplusplus)
extern "C" {
#endif  
  
  //@{
  
  /* ***************************************** */
  /*         Life-Cycle Management             */
  /* ***************************************** */    
  
  /** Initialize coupling platform (boot code only). */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Init_boot(MPICPL* cpl,              /**< [out] MPICPL handle */
		       MPI_Comm worldcomm,       /**< [in] ??? */
		       const char * id,          /**< [in] ??? */
		       int detached,             /**< [in] ??? */
		       int debug                 /**< [in] ??? */
		       );   
  
  /** Run the coupling platform (boot code only). */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Run_boot(MPICPL cpl                 /**< [in] MPICPL handle */
		      );
  
  /** Finalize coupling platform (boot code only). */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Finalize_boot(MPICPL cpl            /**< [in] MPICPL handle */
			   );
       
  /* ***************************************** */
  /*         Code Coupling Description         */
  /* ***************************************** */    
  
  /** Add a new code in the coupling desc. (boot code only). */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Add_code(MPICPL cpl,                /**< [in] MPICPL handle */
		      const char* code_id,       /**< [in] ??? */
		      const char* program,       /**< [in] ??? */
		      const char* cwd,           /**< [in] ??? */
		      int np,                    /**< [in] ??? */
		      const char* args);         /**< [in] ??? */
  
  /** Add a new binding in the coupling desc. (boot code only). */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Add_binding(MPICPL cpl,             /**< [in] MPICPL handle */
			 const char* binding_id, /**< [in] ??? */
			 const char* client,     /**< [in] ??? */ 
			 const char* server      /**< [in] ??? */
			 );
  
  /** Load XML file desc. (boot code only).*/
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Load(MPICPL cpl,                    /**< [in] MPICPL handle */
		  char* xmlfile,                 /**< [in] ??? */
		  char* xmlargv[],               /**< [in] ??? */
    		  char* dtdfile                  /**< [in] ??? */
		  );
  
  /* ***************************************** */
  /*              Request Management           */
  /* ***************************************** */    
  
  /** Wait a request from spawn codes (boot code only). */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Wait(MPICPL cpl,                    /**< [in] MPICPL handle */
		  int * request,                 /**< [in] ??? */
		  char * message                 /**< [in] ??? */
		  );  
  
  /* ***************************************** */
  /*           Intercommunicators              */
  /* ***************************************** */  
  
  /** Get intercomm with spawn code (boot code only). */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Get_spawncomm(MPICPL cpl,           /**< [in] MPICPL handle */
  			   char* code_id,        /**< [in] ??? */
  			   MPI_Comm* spawncomm   /**< [in] ??? */
			   );
  
  /* ***************************************** */
  /*              Debug                        */
  /* ***************************************** */  
  
  /** Print debug info. (any code) */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Print(MPICPL cpl                    /**< [in] MPICPL handle */
		   );  
  
  //@}
  
#if defined(__cplusplus)
}
#endif

#endif /* __MPICPLB_H__ */
