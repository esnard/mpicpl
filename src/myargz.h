/** 
 * @file  myargz.h                       
 * @brief GNU argz declaration for some portability issue.
 * @author Aurelien Esnard                     
 */

#ifndef __MYARGZ_H__
#define __MYARGZ_H__

/* ***************************************** */
/*                  ARGZ                     */
/* ***************************************** */

// #include <argz.h>

typedef int error_t;
extern error_t argz_create(char * const argv[], char **argz, size_t *argz_len);
extern error_t argz_create_sep(const char *str, int sep, char **argz, size_t *argz_len);
extern size_t argz_count(const char *argz, size_t argz_len);
extern void argz_extract(const char *argz, size_t argz_len, char  **argv);
extern void argz_stringify(char *argz, size_t len, int sep);

#endif
