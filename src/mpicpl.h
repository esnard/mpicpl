/** 
 * MPICPL (API for Spawn Code)             
 *
 * @file  mpicpl.h                       
 * @author Aurelien Esnard                     
 * @defgroup mpicpl API for Spawn Code (mpicpl.h)
 */

#ifndef __MPICPL_H__
#define __MPICPL_H__

#include <mpi.h>
#include "struct.h"

/* keep C++ compilers from getting confused. */
#if defined(__cplusplus)
extern "C" {
#endif  
  
  //@{
  
  /* ***************************************** */
  /*         Life-Cycle Management             */
  /* ***************************************** */    
  
  /** \brief Initialize the MPICPL environment. */
  /** \remark This routine must be called by all processes of all
      spawn codes, but it is not synchronizing. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Init(MPICPL* cpl,                   /**< [out] MPICPL handle */
		  MPI_Comm worldcomm,            /**< [in] world communicator of current code */
		  const char * id                /**< [in] code id */
		  );
  
  /** \brief Run MPICPL the environment. */
  /** This routine interconnects the current code within the MPICPL
      framework. At the end of this call, all communicators provided
      by the MPICPL framework (universe and intercomm) are built and
      ready to be use. */
  /** \remark This routine must be called by all processes of all
      spawn codes and it synchronizes them. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Run(MPICPL cpl                      /**< [in] MPICPL handle */
		 );
  
  /** \brief Finalize MPICPL the environment. */
  /** \remark This routine must be called by all processes of all
      spawn codes and it synchronizes them. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Finalize(MPICPL cpl                 /**< [in] MPICPL handle */
		      );
  
  /* ***************************************** */
  /*              Request Management           */
  /* ***************************************** */    
  
  /** \brief Barrier. */
  /** The MPICPL barrier is an extension of the classic MPI
      barrier. This routine blocks until all processes of all spawn
      codes have reached it. */ 
  /** \remark This routine must be called by all processes of all
      spawn codes and it synchronizes them. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Barrier(MPICPL cpl                  /**< [in] MPICPL handle */
		     ); 
  
  /** \brief Restart all spawn codes. */
  /** Stop and respawn all spawn codes with new parameters. */
  /** \remark This routine must be called by all processes of all
      spawn codes and it synchronizes them. */
  /** \warning Each argument in <tt>new_argv</tt> must not contain space characters. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Restart(MPICPL cpl,                 /**< [in] MPICPL handle */
		     char* new_program,          /**< [in] new program name */
		     char* new_cwd,              /**< [in] new current working directory */
		     int new_np,                 /**< [in] new number of processes */
		     char* new_argv[]            /**< [in] new array of program arguments */
		     );  
  
  /** \brief Send a message to the boot code. */
  /** \remark This routine must be called by all processes of a single
      spawn code. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Message(MPICPL cpl,                 /**< [in] MPICPL handle */
		     char* message               /**< [in] message to send to the boot code */
		     );
  
  /* ***************************************** */
  /*           Intercommunicators              */
  /* ***************************************** */  
  
  /** \brief Get universe intracommunicator. */
  /** The universe communicator regroups all processes of all spawn
      codes. It merges all MPI_COMM_WORLD of all spawn codes. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Get_univcomm(MPICPL cpl,            /**< [in] MPICPL handle */
			  MPI_Comm* univcomm     /**< [out] the universe intracommunicator */
			  );
  
  /** \brief Get intercommunicator with the boot code. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Get_bootcomm(MPICPL cpl,            /**< [in] MPICPL handle */
			  MPI_Comm* bootcomm     /**< [out] intercommunicator with the boot code */
			  );
  
  /** \brief Get intercommunicator with another spawn code. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Get_intercomm(MPICPL cpl,           /**< [in] MPICPL handle */
			   char* binding_id,     /**< [in] binding id between two spawn codes */
			   MPI_Comm* intercomm   /**< [out] intercommunicator for the binding id */
			   );
  
  /* ***************************************** */
  /*         Dynamic Code Management           */
  /* ***************************************** */
  
  /** \brief Establish a communication with a client code. */
  /** The server code that call this routine must be a spawn code
      within the MPICPL environment. */
  /** \remark This routine must be called by all processes of all
      spawn codes and it synchronizes them. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Accept(MPICPL cpl,                  /**< [in] MPICPL handle */
		    int bootport,                /**< [in] port number where the boot code is going to listen for connection */
		    MPI_Comm * intercomm         /**< [out] intercommunicator with client as remote group */
		    );
  
  /** \brief Establish a communication with a server code. */
  /** This server code must be a spawn code within the MPICPL
      environment. However, the client code that call this routine
      dont need to initialize the MPICPL environment. */
  /** \remark This routine must be called by all processes of all
      spawn codes and it synchronizes them. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Connect(MPI_Comm world,             /**< [in] communicator of client code */
		     char * boothostname,        /**< [in] host name of the boot code */
		     int bootport,               /**< [in] port number where the boot code is listening for connection */
		     MPI_Comm * intercomm        /**< [out] intercommunicator with server as remote group */
		     );
  
  /* ***************************************** */
  /*              Debug                        */
  /* ***************************************** */  
  
  /** \brief Print information on MPICPL environment. */
  /** \return integer error status (MPICPL_SUCCESS or MPICPL_ERROR) */
  int MPICPL_Print(MPICPL cpl                    /**< [in] MPICPL handle */
		   );  
  
  //@}
  
#if defined(__cplusplus)
}
#endif

#endif /* __MPICPL_H__ */
