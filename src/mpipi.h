/** 
 * MPIPI (API for advanced MPI2 routines)             
 *
 * @file  mpipi.h                       
 * @author Aurelien Esnard                     
 * @defgroup mpipi API for advanced MPI2 routines
 */

#ifndef __MPIPI_H__
#define __MPIPI_H__

#include <mpi.h>

#define MPIPI_MAX_TRY_CONNECT       5
#define MPIPI_USLEEP_CONNECT        50*1000 /* 50 ms */

/* keep C++ compilers from getting confused. */
#if defined(__cplusplus)
extern "C" {
#endif  
  
  //@{  
  
  /* ***************************************** */
  /*                  MPIPI                    */
  /* ***************************************** */
  
  /** MPI2 client connect routine using MPI2 lookup */
  int MPIPI_Connect(MPI_Comm comm,
		    char * id,
		    char * service_name,
		    char * port_name,       /* out */
		    MPI_Comm * intercomm,   /* out */
		    int debug);
  
  /* ***************************************** */
  
  /** MPI2 server accept routine using MPI2 publish */
  int MPIPI_Accept(MPI_Comm comm,	
		   char * id,
		   char * service_name,
		   char * port_name,       /* out */
		   MPI_Comm * intercomm,   /* out */
		   int debug);  
  
  /* ***************************************** */
  
  //@}
  
#if defined(__cplusplus)
}
#endif

#endif /* __MPIPI_H__ */
