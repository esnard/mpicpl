!     @file  mpicplf.h                       
!     @brief MPICPL Fortran Parameters
!     @author Aurelien Esnard                     

      integer*4 MPICPL_SUCCESS
      parameter (MPICPL_SUCCESS = MPI_SUCCESS)  
      integer*4 MPICPL_ERROR
      parameter (MPICPL_ERROR = -1)  


