/* MPI Coupling Library (F77 API)            */
/* Filename: mpicplf.c                       */
/* Author(s): A. Esnard                      */

#include "mpi.h"
#include "mpicpl.h"
#include "mpicplb.h"
#include "assert.h"

/* Fortran handle */
typedef long * MPICPL_Handle; 

/* Convert Fortran handle to a C handle */
#define F2C(handle_f) ((void*)(*(handle_f)))

/* Convert C handle to Fortran handle. */
#define C2F(handle_c,handle_f) (*(handle_f) = (long)(handle_c))

/* The name mangling schemes. */
#ifdef MPICPL_SECOND_UNDERSCORE
#define F77_FUNC(name,NAME) name ## __
#else
#define F77_FUNC(name,NAME) name ## _
#endif

/* keep C++ compilers from getting confused. */
#if defined(__cplusplus)
extern "C" { 
#endif
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_init, MPICPL_INIT)
       (MPICPL_Handle cpl, 
	MPI_Fint * fcomm, 
	char * id, 
	int * ierr)
  {
    assert(sizeof(int) == 4); /* debug */
    MPI_Comm comm = MPI_Comm_f2c(*fcomm);
    assert(comm == MPI_COMM_WORLD);
    MPICPL _cpl;
    *ierr = MPICPL_Init(&_cpl,comm,id);
    C2F(_cpl,cpl);     
  }
  
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_init_boot, MPICPL_INIT_BOOT)
       (MPICPL_Handle cpl, 
	MPI_Fint * fcomm, 
	char * id, 
	int * detached,
	int * debug,
	int * ierr)
  {
    assert(sizeof(int) == 4); /* debug */
    MPI_Comm comm = MPI_Comm_f2c(*fcomm);
    assert(comm == MPI_COMM_WORLD);
    MPICPL _cpl;
    *ierr = MPICPL_Init_boot(&_cpl,comm,id,*detached, *debug);
    C2F(_cpl,cpl);     
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_finalize, MPICPL_FINALIZE)
       (MPICPL_Handle cpl, /* in */ 
	int * ierr)        /* out */
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Finalize(_cpl);
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_finalize_boot, MPICPL_FINALIZE_BOOT)
       (MPICPL_Handle cpl, /* in */ 
	int * ierr)            /* out */
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Finalize(_cpl);
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_add_code, MPICPL_ADD_CODE)
       (MPICPL_Handle cpl, 
	char * code_id, 
	char * program, 
	char * cwd, 
	int * np, 
	char* args, 
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Add_code(_cpl, code_id, program, cwd, *np, args);
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_add_binding, MPICPL_ADD_BINDING)
       (MPICPL_Handle cpl, 
	char* binding_id, 
	char* client, 
	char* server, 
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Add_binding(_cpl, binding_id, client, server);
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_run, MPICPL_RUN)
       (MPICPL_Handle cpl, 
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Run(_cpl);
  }
  
  /* ***************************************** */  
  
  void F77_FUNC(mpicpl_run_boot, MPICPL_RUN_BOOT)
       (MPICPL_Handle cpl, 
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Run(_cpl);
  }
  
  /* ***************************************** */  
  
  /* void F77_FUNC(mpicpl_synchronize, MPICPL_SYNCHRONIZE) */
  /*      (MPICPL_Handle cpl,  */
  /* 	int * ierr) */
  /* { */
  /*   MPICPL _cpl = (MPICPL)F2C(cpl); */
  /*   *ierr = MPICPL_Synchronize(_cpl); */
  /* } */
  
  
  /* ***************************************** */  
  
  void F77_FUNC(mpicpl_message, MPICPL_MESSAGE)
       (MPICPL_Handle cpl, 
	char * message,
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Message(_cpl, message);
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_wait, MPICPL_WAIT)
       (MPICPL_Handle cpl, 
	int * request,  /* out */
	char * message, /* out */
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Wait(_cpl, request, message);
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_restart, MPICPL_RESTART)
       (MPICPL_Handle cpl, 
	char* new_program, 
	char* new_cwd, 
	int * new_np, 
	char* new_argv[], /* how do we write it ? */
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Restart(_cpl, new_program, new_cwd, *new_np, new_argv); 
  }  
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_get_bootcomm, MPICPL_GET_BOOTCOMM)
       (MPICPL_Handle cpl, 
	MPI_Fint * fbootcomm, /* out */
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    MPI_Comm bootcomm;
    *ierr = MPICPL_Get_bootcomm(_cpl, &bootcomm);
    *fbootcomm = MPI_Comm_c2f(bootcomm);
    
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_get_spawncomm, MPICPL_GET_SPAWNCOMM)
       (MPICPL_Handle cpl, 
	char * code_id, 
	MPI_Fint * fspawncomm, /* out */
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    MPI_Comm spawncomm;
    *ierr = MPICPL_Get_spawncomm(_cpl, code_id, &spawncomm);
    *fspawncomm = MPI_Comm_c2f(spawncomm);    
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_get_intercomm, MPICPL_GET_INTERCOMM)
       (MPICPL_Handle cpl, 
	char * binding_id, 
	MPI_Fint * fintercomm, /* out */
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    MPI_Comm intercomm;
    *ierr = MPICPL_Get_intercomm(_cpl,binding_id, &intercomm);
    *fintercomm = MPI_Comm_c2f(intercomm);
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_get_univcomm, MPICPL_GET_UNIVCOMM)
       (MPICPL_Handle cpl, 
	MPI_Fint * funivcomm, /* out */
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    MPI_Comm univcomm;
    *ierr = MPICPL_Get_univcomm(_cpl, &univcomm);
    *funivcomm = MPI_Comm_c2f(univcomm);
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_barrier, MPICPL_BARRIER)       
       (MPICPL_Handle cpl, 
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Barrier(_cpl);
  }
  
  /* ***************************************** */
  
  void F77_FUNC(mpicpl_print, MPICPL_PRINT)       
       (MPICPL_Handle cpl, 
	int * ierr)
  {
    MPICPL _cpl = (MPICPL)F2C(cpl);
    *ierr = MPICPL_Print(_cpl);
  }
  
  /* ***************************************** */
  
#if defined(__cplusplus)
}
#endif

/* EOF */
