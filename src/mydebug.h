/** 
 * @file  mydebug.h                       
 * @brief MPICPL Debug Tools
 * @author Aurelien Esnard                     
 */

#ifndef __MYDEBUG_H__
#define __MYDEBUG_H__

/* ***************************************** */
/*                 DEBUG                     */
/* ***************************************** */

/* debug level */
#define MPICPL_DEBUG_NO              0
#define MPICPL_DEBUG_LOW             1
#define MPICPL_DEBUG_HIGH            2
#define MPICPL_DEBUG_ERROR           -1  /* force */

#ifdef DEBUG

// #define BLACK   "\033[30m"
#define BLACK   "\033[0m"
#define RED     "\033[31m"
#define GREEN   "\033[32m"
#define ORANGE  "\033[33m"
#define BLUE    "\033[34m"
#define MAGENTA "\033[35m"
#define CYAN    "\033[36m"
#define WHITE   "\033[37m"

#define PRINT(debug, level, id, rank, color, str, ...)		       \
  do {								       \
    if(debug >= level) {					       \
      fprintf(stderr, "%s", color);				       \
      fprintf(stderr, "[%s:%d] ", id, rank);			       \
      fprintf(stderr, str, ##__VA_ARGS__);			       \
      fprintf(stderr, "%s", BLACK);				       \
      fflush(stderr);                                                  \
    }								       \
  } while(0)

#define PRINT_ERROR(debug, id, rank, str, ...) PRINT(debug, MPICPL_DEBUG_ERROR, id, rank, RED, str, ##__VA_ARGS__)
#define PRINT_WARNING(debug, id, rank, str, ...) PRINT(debug, MPICPL_DEBUG_ERROR, id, rank, ORANGE, str, ##__VA_ARGS__)
#define PRINT_LOW(debug, id, rank, str, ...) PRINT(debug, MPICPL_DEBUG_LOW, id, rank, BLACK, str, ##__VA_ARGS__)
#define PRINT_HIGH(debug, id, rank, str, ...) PRINT(debug, MPICPL_DEBUG_HIGH, id, rank, BLACK, str, ##__VA_ARGS__)

#define MPICPL_PRINT_ERROR(cpl, str, ...) PRINT_ERROR((cpl)->debug, (cpl)->id, (cpl)->prank, str, ##__VA_ARGS__)
#define MPICPL_PRINT_WARNING(cpl, str, ...) PRINT_WARNING((cpl)->debug, (cpl)->id, (cpl)->prank, str, ##__VA_ARGS__)
#define MPICPL_PRINT_LOW(cpl, str, ...) PRINT_LOW((cpl)->debug, (cpl)->id, (cpl)->prank, str, ##__VA_ARGS__)
#define MPICPL_PRINT_HIGH(cpl, str, ...) PRINT_HIGH((cpl)->debug, (cpl)->id, (cpl)->prank, str, ##__VA_ARGS__)

#else

/* NO DEBUG */

#define PRINT_ERROR(debug, id, rank, str, ...)  do { /* nothing */ } while(0)
#define PRINT_WARNING(debug, id, rank, str, ...)  do { /* nothing */ } while(0)
#define PRINT_LOW(debug, id, rank, str, ...)  do { /* nothing */ } while(0)
#define PRINT_HIGH(debug, id, rank, str, ...)  do { /* nothing */ } while(0)

#define MPICPL_PRINT_ERROR(cpl, str, ...)  do { /* nothing */ } while(0)
#define MPICPL_PRINT_WARNING(cpl, str, ...)  do { /* nothing */ } while(0)
#define MPICPL_PRINT_LOW(cpl, str, ...)  do { /* nothing */ } while(0)
#define MPICPL_PRINT_HIGH(cpl, str, ...)  do { /* nothing */ } while(0)

#endif

#endif
