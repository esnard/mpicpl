/* Copyright (C) 1996, 1997 Free Software Foundation, Inc.
   This file is part of the GNU C Library.
   Contributed by Ulrich Drepper <drepper@gnu.ai.mit.edu>, 1996.
   
   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.
   
   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.
   
   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#define _GNU_SOURCE
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

#include "myargz.h"

/* Find the length of S, but scan at most MAXLEN characters.  If no
   '\0' terminator is found in that many characters, return MAXLEN.  */
size_t strnlen (const char *s, size_t n)
{
  const char *p = (const char *)memchr(s, 0, n);
  return(p ? p-s : n);
}

/* Make a '\0' separated arg vector from a unix argv vector, returning it in
   ARGZ, and the total length in LEN.  If a memory allocation error occurs,
   ENOMEM is returned, otherwise 0.  */
error_t argz_create (char *const argv[], char **argz, size_t *len)
{
  int argc;
  size_t tlen = 0;
  char *const *ap;
  char *p;
  
  for (argc = 0; argv[argc] != NULL; ++argc)
    tlen += strlen (argv[argc]) + 1;
  
  if (tlen == 0)
    *argz = NULL;
  else
    {
      *argz = malloc (tlen);
      if (*argz == NULL)
	return ENOMEM;
      
      for (p = *argz, ap = argv; *ap; ++ap, ++p)
	p = stpcpy (p, *ap);
    }
  *len = tlen;
  
  return 0;
}

error_t argz_create_sep (const char *string, int delim, char **argz, size_t *len)
{
  size_t nlen = strlen (string) + 1;
  
  if (nlen > 1)
    {
      const char *rp;
      char *wp;
      
      *argz = (char *) malloc (nlen);
      if (*argz == NULL)
	return ENOMEM;
      
      rp = string;
      wp = *argz;
      do
	if (*rp == delim)
	  {
	    if (wp > *argz && wp[-1] != '\0')
	      *wp++ = '\0';
	    else
	      --nlen;
	  }
	else
	  *wp++ = *rp;
      while (*rp++ != '\0');
      
      if (nlen == 0)
	{
	  free (*argz);
	  *argz = NULL;
	  *len = 0;
	}
      
      *len = nlen;
    }
  else
    {
      *argz = NULL;
      *len = 0;
    }
  
  return 0;
}

/* Returns the number of strings in ARGZ.  */
size_t argz_count (const char *argz, size_t len)
{
  size_t count = 0;
  while (len > 0)
    {
      size_t part_len = strlen(argz);
      argz += part_len + 1;
      len -= part_len + 1;
      count++;
    }
  return count;
}

/* Puts pointers to each string in ARGZ, plus a terminating 0 element, into
   ARGV, which must be large enough to hold them all.  */
void argz_extract (const char *argz, size_t len, char **argv)
{
  while (len > 0)
    {
      size_t part_len = strlen (argz);
      *argv++ = (char *) argz;
      argz += part_len + 1;
      len -= part_len + 1;
    }
  *argv = 0;
}


/* Make '\0' separated arg vector ARGZ printable by converting all the '\0's
   except the last into the character SEP.  */
void argz_stringify (char *argz, size_t len, int sep)
{
  if (len > 0)
    while (1)
      {
	size_t part_len = strnlen (argz, len);
	argz += part_len;
	len -= part_len;
	if (len-- <= 1)		/* includes final '\0' we want to stop at */
	  break;
	*argz++ = sep;
      }
}

