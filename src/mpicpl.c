/* MPI Coupling Library                      */
/* Filename: mpicpl.c                        */
/* Author(s): A. Esnard                      */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>

/* sockets & threads */
#include <sys/types.h>        
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>

#define __USE_BSD
#include <unistd.h> /* usleep */

#include <mpi.h>

#include "mpicpl.h"
#include "mpicplb.h" /* to remove from here !!! */
#include "mpipi.h"
#include "struct.h"
#include "mydebug.h"
#include "myargz.h"

#define NB_REQUESTS 5
const char * REQUESTS[NB_REQUESTS] = {"END", "SYNCHRONIZE", "RESTART", "MESSAGE", "LISTEN"};

/* ***************************************** */
/*                 PRIVATE                   */
/* ***************************************** */

static 
int MPICPL_Find_code(MPICPL cpl, 
		     char * id, 
		     MPICPL_Code * code,
		     int * crank)
{
  assert(cpl != NULL);
  if(code != NULL) *code = NULL;
  for(int i = 0 ; i < cpl->nb_codes ; i++) 
    if(strcmp(id, cpl->codes[i]->id) ==0) {
      if(code != NULL) *code = cpl->codes[i];
      if(crank != NULL) *crank = i;
      break;
    }  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

static 
int MPICPL_Find_binding(MPICPL cpl, 
			char * id, 
			MPICPL_Binding * binding,
			int * brank)
{
  assert(cpl != NULL);
  if(binding != NULL) *binding = NULL;
  for(int i = 0 ; i < cpl->nb_bindings ; i++) 
    if(strcmp(id, cpl->bindings[i]->id) ==0) {
      if(binding != NULL) *binding = cpl->bindings[i];
      if(brank != NULL) *brank = i;
      break;
    }
  return MPICPL_SUCCESS;
}

/* ***************************************** */

static
int MPICPL_Synchronize_internal(MPICPL cpl)
{
  int err;
  
  /* from boot code */
  if(cpl->kind == MPICPL_BOOT_CODE) {    
    /* for all spawn codes */
    for(int i = 0 ; i < cpl->nb_codes ; i++) {
      err = MPI_Barrier(cpl->codes[i]->bootcomm); 
      assert(err == MPI_SUCCESS);
      MPICPL_PRINT_HIGH(cpl, "INTERNAL SYNCHRO OF CODE %s\n", cpl->codes[i]->id);
    }
  }
  
  /* from spawn code */
  else {
    err = MPI_Barrier(cpl->parentcomm); 
    assert(err == MPI_SUCCESS);
    MPICPL_PRINT_HIGH(cpl, "INTERNAL SYNCHRO OF CODE %s\n", cpl->id);
  }
  
  return MPICPL_SUCCESS;  
}

/* ***************************************** */

static
int MPICPL_Spawn_code(MPICPL cpl, 
		      MPICPL_Code code)
{
  assert(cpl != NULL && cpl->kind == MPICPL_BOOT_CODE);
  MPICPL_PRINT_LOW(cpl, "SPAWN CODE %s, NP = %d, PROGRAM = %s, CWD = %s, ARGS = %s\n", 
		   code->id, code->np, code->program, code->cwd, code->args);
  
  /* prepare args */
  char * argz;
  size_t argz_len; /* full string length */
  /* WARNING: an error will occur if argv[i] contains a white space! */
  argz_create_sep(code->args, ' ', &argz, &argz_len); 
  size_t argc = argz_count(argz, argz_len); /* nb of substrings */
  char *argv[argc+1];
  argz_extract(argz, argz_len, argv);
  
  /* set cwd */  
  MPI_Info info = MPI_INFO_NULL;
  MPI_Info_create(&info);
  MPI_Info_set(info, "wdir", code->cwd); /* MPI-2 Standard (?) */ 
  /* OpenMPI Warning ==> LOCAL DAEMON SPAWN IS CURRENTLY UNSUPPORTED ??? */
  
  /* check if cwd is accessible */
  if(access(code->cwd, R_OK | X_OK) != 0) {
    MPICPL_PRINT_ERROR(cpl, "ERROR: CAN'T ACCESS CWD %s FOR SPAWN CODE %s\n",
		       code->cwd, code->id);
    MPI_Abort(cpl->worldcomm, 1);
    return MPICPL_ERROR;
  }
  
  /* check if program is accessible */
  if(access(code->program, R_OK | X_OK) != 0) {
    MPICPL_PRINT_ERROR(cpl, "ERROR: CAN'T ACCESS PROGRAM %s FOR SPAWN CODE %s\n",
		       code->program, code->id);
    MPI_Abort(cpl->worldcomm, 1);
    return MPICPL_ERROR;
  }
  
  /* disable MPI fatal errors */
  MPI_Comm_set_errhandler(cpl->worldcomm,  MPI_ERRORS_RETURN);
  
  int err;
  int errcodes[code->np];
  int root = 0;
  
  /* spawn the code */
  err = MPI_Comm_spawn(code->program, 
		       argv,            /* MPI_ARGV_NULL */
		       code->np, 
		       info,            /* or MPI_INFO_NULL */
		       root,            /* root rank 0 for spawn */
		       cpl->worldcomm,  /* or MPI_COMM_WORLD or MPI_COMM_SELF ? */
		       &code->bootcomm, 
		       errcodes);       /* or MPI_ERRCODES_IGNORE */
  
  assert(err == MPI_SUCCESS);
  
  /* free argz mem */
  free(argz);
  
  /* MPI fatal errors (default) */
  MPI_Comm_set_errhandler(cpl->worldcomm, MPI_ERRORS_ARE_FATAL);
  
  /* check spawn errors */
  if(cpl->prank == root) {
    int ok = 1;
    for(int i = 0 ; i < code->np ; i++) 
      if(errcodes[i] != MPI_SUCCESS) {
	ok = 0;
	char errstr[MPICPL_MAX_CHAR]; int errlen;
	MPI_Error_string(errcodes[i], errstr, &errlen);
	MPICPL_PRINT_ERROR(cpl, "ERROR: SPAWN CODE %s:%d, %s (MPI errcode = %d)\n", 
			   code->id, i, errstr, errcodes[i]);
      }
    if(!ok) MPI_Abort(cpl->worldcomm, 1);
  }
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

/* must run both on boot code and client spawn code */
/* portname need to known only on client root and */

static
int MPICPL_Lookup_binding(MPICPL cpl,			  
			  char * service_name,
			  char * portname,   /* out: written only on root of client code */
			  MPI_Comm bootcomm)  /* in: intercomm betweeen boot code and client code */
{
  assert(cpl != NULL);
  
  /* boot code (send portname to client code)*/
  
  if(cpl->kind == MPICPL_BOOT_CODE && cpl->prank == 0) {
    int root = 0;
    MPI_Send(portname, MPI_MAX_PORT_NAME, MPI_CHAR, root, MPICPL_PUBLISH_TAG, bootcomm); 
  }
  
  /* client code (recv portname fromm boot code) */
  
  else {
    int root = 0;
    if(cpl->prank == 0) {
      MPI_Status status;
      MPI_Recv(portname, MPI_MAX_PORT_NAME, MPI_CHAR, root, MPICPL_PUBLISH_TAG, bootcomm, &status); 
      MPICPL_PRINT_HIGH(cpl, "LOOKUP SERVICE %s ON CLIENT CODE (%s)\n", service_name, portname);
    }
  }  
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */


/* must run both on boot code and server spawn code */

static
int MPICPL_Publish_binding(MPICPL cpl, 
			   char * service_name,
			   char * portname,  /* out: stored only on root of boot/server codes */
			   MPI_Comm bootcomm) /* in: intercomm betweeen boot code and server code */
{
  assert(cpl != NULL);
  
  /* boot code (recv portname from server code)*/
  
  if(cpl->kind == MPICPL_BOOT_CODE && cpl->prank == 0) {
    int root = 0;
    MPI_Status status;
    MPI_Recv(portname, MPI_MAX_PORT_NAME, MPI_CHAR, root, MPICPL_PUBLISH_TAG, bootcomm, &status); // bootcomm = cpl->parentcomm
    MPICPL_PRINT_HIGH(cpl, "PUBLISHING SERVICE %s ON BOOT CODE (%s)\n", service_name, portname);       
  }
  
  /* server code (send portname to boot code) */
  
  else {
    
    /* open port on rank 0 of server comm */        
    if (cpl->prank == 0) {
      int err = MPI_Open_port(MPI_INFO_NULL, portname); /* out */
      MPICPL_PRINT_HIGH(cpl, "OPEN PORT %s FOR SERVICE %s\n", portname, service_name);
      assert(err == MPI_SUCCESS);
      int root = 0;
      MPI_Send(portname, MPI_MAX_PORT_NAME, MPI_CHAR, root, MPICPL_PUBLISH_TAG, bootcomm);  // bootcomm = cpl->parentcomm
    }
  }
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

/* run on server code only */

static
int MPICPL_Accept_binding(MPICPL cpl, 
			  MPI_Comm comm,
			  char * service_name,    /* in */
			  char * portname,       /* in: used only on server root */
			  MPI_Comm * intercomm)   /* out */
{
  assert(cpl != NULL);    
  assert(cpl->kind == MPICPL_SPAWN_CODE);
  int root = 0;
  
  /* wait connection */
  MPICPL_PRINT_HIGH(cpl, "ACCEPTING CONNECTION FOR SERVICE %s...\n", service_name);
  int err = MPI_Comm_accept(portname,   /* in */
			    MPI_INFO_NULL,
			    root,
			    comm,
			    intercomm);  /* out */
  
  if(err != MPI_SUCCESS) {
    MPICPL_PRINT_ERROR(cpl, "ERROR: ACCEPTING CONNECTION FOR SERVICE %s...\n", service_name);
    MPI_Abort(comm, 1);
  }
  
  MPICPL_PRINT_LOW(cpl, "CONNECTION ESTABLISHED FOR SERVICE %s\n", service_name);
  
  return MPICPL_SUCCESS;      
}

/* ***************************************** */

/* run on client code only */

static
int MPICPL_Connect_binding(MPICPL cpl,
			   MPI_Comm comm,
			   char * service_name,
			   char * portname,       /* in: used only on client root */
			   MPI_Comm * intercomm)   /* out */
{
  assert(cpl != NULL);  
  assert(cpl->kind == MPICPL_SPAWN_CODE);
  int root = 0;
  
  /* establishes communication with server */
  MPICPL_PRINT_HIGH(cpl, "CONNECTING TO SERVICE %s...\n", service_name);
  int err = MPI_Comm_connect(portname,  /* used only on root */
			     MPI_INFO_NULL,
			     root,
			     comm,
			     intercomm);
  
  if(err != MPI_SUCCESS) {
    MPICPL_PRINT_WARNING(cpl, "WARNING: FAIL TO CONNECT TO SERVICE %s\n", service_name);
    return err;
  }
  
  MPICPL_PRINT_LOW(cpl, "CONNECTION ESTABLISHED FOR SERVICE %s\n", service_name);  
  return MPICPL_SUCCESS;    
}


/* ***************************************** */

static
int MPICPL_Interconnect_codes(MPICPL cpl,
			      MPICPL_Binding binding)
{
  assert(cpl != NULL);
  
  MPICPL_Code client, server;
  MPICPL_Find_code(cpl, binding->client, &client, NULL);
  MPICPL_Find_code(cpl, binding->server, &server, NULL);
  assert(client != NULL);
  assert(server != NULL);
  
  /* from boot code... (sender) */
  if(cpl->kind == MPICPL_BOOT_CODE) {
    int root = (cpl->prank == 0)?MPI_ROOT:MPI_PROC_NULL;
    
    /* broadcast request to server code for connection */
    MPICPL_PRINT_HIGH(cpl, "BINDING %s: REQUEST SERVER %s\n", binding->id, binding->server);
    MPI_Bcast(binding->id, MPICPL_MAX_CHAR, MPI_CHAR, root, server->bootcomm); 
    MPICPL_Publish_binding(cpl, binding->id, binding->portname, server->bootcomm); 
    
    /* short pause to help server to start before client */
    usleep(MPICPL_USLEEP_CONNECT); 
    
    /* broadcast request to client code for connection */
    MPICPL_PRINT_HIGH(cpl, "BINDING %s: REQUEST CLIENT %s\n", binding->id, binding->client);
    MPI_Bcast(binding->id, MPICPL_MAX_CHAR, MPI_CHAR, root, client->bootcomm); 
    MPICPL_Lookup_binding(cpl, binding->id, binding->portname, client->bootcomm); 
    
  } /* end of boot code */
  
  /* from spawn code... */
  else {
    MPICPL_PRINT_LOW(cpl, "INTERCONNECT CODES %s-%s (BINDING %s)\n", 
		     binding->client, binding->server, binding->id);  
    
    /* server code (receiver) */
    if(strcmp(cpl->id, binding->server) == 0) {
      int root = 0;
      char id[MPICPL_MAX_CHAR];
      
      /* broadcast request to server code for connection */
      MPI_Bcast(id, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->parentcomm); 
      
      /* publish port name in boot code */
      int err = MPICPL_Publish_binding(cpl, binding->id, binding->portname, cpl->parentcomm);       
      assert(err == MPICPL_SUCCESS);
      
      /* accept connections, waiting... */
      err = MPICPL_Accept_binding(cpl, cpl->worldcomm, binding->id, binding->portname, &binding->intercomm);
      assert(err == MPICPL_SUCCESS);      
    }
    
    /* client code (receiver) */
    if(strcmp(cpl->id, binding->client) == 0) {
      int root = 0;
      char id[MPICPL_MAX_CHAR];
      
      /* broadcast request to client code for connection */
      MPI_Bcast(id, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->parentcomm);         
      
      /* lookup port name on boot code */
      int err = MPICPL_Lookup_binding(cpl, binding->id, binding->portname, cpl->parentcomm);      
      assert(err == MPICPL_SUCCESS);
      
      /* connect */
      err = MPICPL_Connect_binding(cpl, cpl->worldcomm, binding->id, binding->portname, &binding->intercomm);
      
      /* check */
      if(err != MPI_SUCCESS) {
	MPICPL_PRINT_ERROR(cpl, "FAILED TO CONNECT FOR BINDING %s\n", binding->id);
	MPI_Abort(cpl->worldcomm, 1);
      }       
    }   
    
  } /* end of spawn codes */
  
  return MPICPL_SUCCESS;  
}

/* ***************************************** */

/* expand = interconnect one more code in universe */

static
int MPICPL_Expand_universe(MPICPL cpl,
			   MPI_Comm worldcomm, /* different for client, server and boot code (for server, code comms partially merged) */
			   int nb_codes,       /* nb codes = nb server codes + 1 client code */
			   char * binding_id,
			   char * binding_client, 
			   char * binding_server[],
			   MPI_Comm * binding_intercomm)
{
  assert(cpl != NULL);
  assert(nb_codes >= 2);
  int nb_servers = nb_codes - 1;
  
  *binding_intercomm = MPI_COMM_NULL;
  
  /* rank in worldcomm */
  
  int prank;
  MPI_Comm_rank(worldcomm, &prank); 
  
  /* id for servers */
  
  char binding_servers[1024] = "\0";
  for(int i = 0 ; i < nb_servers ; i++) {
    strcat(binding_servers, binding_server[i]);
    if(i < nb_servers-1) strcat(binding_servers, "|");
  }
  
  MPICPL_PRINT_HIGH(cpl, "UNIVERSE: BINDING %s: EXPANDING WITH CODES %s-%s\n", binding_id, binding_client, binding_servers);
  
  /* get client & server codes */
  
  MPICPL_Code client, servers[nb_servers];
  MPICPL_Find_code(cpl, binding_client, &client, NULL);
  assert(client != NULL);
  
  for(int i = 0 ; i < nb_servers ; i++) {
    MPICPL_Find_code(cpl, binding_server[i], &servers[i], NULL);
    assert(servers[i] != NULL);
  }
  
  /* is client or server or boot code ? */
  
  int is_client = (strcmp(cpl->id, binding_client) == 0);
  int is_server = 0; 
  for(int i = 0 ; i < nb_servers ; i++) 
    if(strcmp(cpl->id, binding_server[i]) == 0) 
      { is_server = 1; break; }
  
  /* from boot code... (sender) */
  if(cpl->kind == MPICPL_BOOT_CODE) {
    
    int root = (prank == 0)?MPI_ROOT:MPI_PROC_NULL;
    
    /* broadcast request to all server codes for connection */
    for(int i = 0 ; i < nb_servers ; i++) {
      MPICPL_PRINT_HIGH(cpl, "UNIVERSE: BINDING %s: REQUEST SERVER %s\n", binding_id, binding_server[i]);
      MPI_Bcast(binding_id, MPICPL_MAX_CHAR, MPI_CHAR, root, servers[i]->bootcomm); 
    }   
    
    /* publish */
    if(prank == 0) {
      int err = MPICPL_Publish_binding(cpl, binding_id, client->univ_portname, servers[0]->bootcomm);       
      assert(err == MPICPL_SUCCESS);
    }
    
    /* short pause to help server to start before client */
    usleep(MPICPL_USLEEP_CONNECT); 
    
    /* broadcast request to client code for connection */
    MPICPL_PRINT_HIGH(cpl, "UNIVERSE: BINDING %s: REQUEST CLIENT %s\n", binding_id, binding_client);
    MPI_Bcast(binding_id, MPICPL_MAX_CHAR, MPI_CHAR, root, client->bootcomm); 
    
    /* lookup */
    if(prank == 0) {
      int err = MPICPL_Lookup_binding(cpl, binding_id, client->univ_portname, client->bootcomm);       
      assert(err == MPICPL_SUCCESS);
    }    
    
  }
  
  /* from spawn code... */
  else {
    
    /* server code (receiver) */
    if(is_server) {
      int root = 0;
      char id[MPICPL_MAX_CHAR];
      
      MPICPL_PRINT_LOW(cpl, "UNIVERSE: BINDING %s: SERVER WAITING REQUEST...\n", binding_id);  
      
      /* broadcast request to server code for connection */
      MPI_Bcast(id, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->parentcomm);       
      MPICPL_PRINT_LOW(cpl, "UNIVERSE: BINDING %s: SERVER ACCEPTING...\n", binding_id);  
      
      /* publish */
      if(prank == 0) {
	int err = MPICPL_Publish_binding(cpl, binding_id, client->univ_portname, cpl->parentcomm);       
	assert(err == MPICPL_SUCCESS);
      }
      
      
      /* accept connections, waiting... */
      int err = MPICPL_Accept_binding(cpl, worldcomm, binding_id, client->univ_portname, binding_intercomm);
      assert(err == MPICPL_SUCCESS);      
    }
    
    /* client code (receiver) */
    if(is_client) {
      int root = 0;
      char id[MPICPL_MAX_CHAR];
      
      MPICPL_PRINT_LOW(cpl, "UNIVERSE: BINDING %s: CLIENT WAITING REQUEST...\n", binding_id);  
      
      /* broadcast request to client code for connection */
      MPI_Bcast(id, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->parentcomm);
      
      /* lookup */
      if(prank == 0) {
	int err = MPICPL_Lookup_binding(cpl, binding_id, client->univ_portname, cpl->parentcomm);       
	assert(err == MPICPL_SUCCESS);
      }      
      
      /* connect */
      int err = MPICPL_Connect_binding(cpl, worldcomm, binding_id, client->univ_portname, binding_intercomm);
      
      /* check */
      if(err != MPICPL_SUCCESS) {
	MPICPL_PRINT_ERROR(cpl, "UNIVERSE: FAILED TO CONNECT FOR BINDING %s\n", binding_id);
	MPI_Abort(worldcomm, 1);
      } 
      
    }       
  }
  
  return MPICPL_SUCCESS;  
}

/* ***************************************** */

static
int MPICPL_Interconnect_universe(MPICPL cpl, 
				 MPI_Comm * universe) 
{    
  /* Merge all codes in universe intracomm, by adding one code at each
     step. Lets consider N codes A, B, C, ... At step 0, A is
     connected by B to create intercomm A|B ; then, at step 1, A|B is
     connected by C to create intercomm A|B|C ; and so on...
  */
  
  MPI_Comm intracomm = MPI_COMM_NULL;
  MPI_Comm_dup(cpl->worldcomm, &intracomm);
  
  /* code ids */
  const char * codes[cpl->nb_codes];
  for(int i = 0 ; i < cpl->nb_codes ; i++) {
    codes[i] =  cpl->codes[i]->id;
  }
  
  int isBoot = cpl->kind == MPICPL_BOOT_CODE;
  
  /* for all codes */
  for(int i = 1 ; i < cpl->nb_codes ; i++) {     
    
    int nb_servers = i; /* nb servers at step i */
    char * client = (char*)codes[i];
    char ** servers = (char**)codes; /* {codes[0], ..., codes[i-1]} */
    char binding[MPICPL_MAX_CHAR];
    sprintf(binding,"UNIVERSE-%d",i-1); // UNIVERSE-0, UNIVERSE-1, ...
    
    /* is client or server or boot code ? */
    
    int isClient = (strcmp(cpl->id, client) == 0);
    int isServer = 0; 
    for(int k = 0 ; k < nb_servers ; k++) 
      if(strcmp(cpl->id, servers[k]) == 0) 
	{ isServer = 1; break; }    
    
    if(isClient || isServer || isBoot) {
      MPICPL_PRINT_HIGH(cpl, "UNIVERSE: EXPAND %s\n", binding);
      MPI_Comm intercomm = MPI_COMM_NULL;
      MPI_Comm worldcomm = isServer?intracomm:cpl->worldcomm;
      /* get new intercomm with codes[i] as client */
      MPICPL_Expand_universe(cpl,
			     worldcomm,
			     nb_servers+1, 
			     binding,
			     client, servers,
			     &intercomm);
      
      if(!isBoot) {    
	MPICPL_PRINT_HIGH(cpl, "UNIVERSE: INTERCOMM BARRIER %s\n", binding);
	MPI_Barrier(intercomm);
	
	MPICPL_PRINT_HIGH(cpl, "UNIVERSE: INTERCOMM MERGE %s\n", binding);
	if(isServer) MPI_Comm_free(&intracomm); /* free old intracomm */
	MPI_Intercomm_merge(intercomm, isClient, &intracomm); /* new intracomm */	
	MPI_Comm_disconnect(&intercomm);
      }
      
    }
    
    /* synchro */
    MPICPL_Synchronize_internal(cpl); // useful ?
    
  }
  
  /* universe */
  
  if(!isBoot) {
    MPI_Comm_dup(intracomm, universe);  
    MPI_Comm_free(&intracomm);
  }
  else
    *universe = MPI_COMM_NULL;  
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

static
int MPICPL_Bcast_desc(MPICPL cpl)
{
  
  /* from boot code... (sender) */
  if(cpl->kind == MPICPL_BOOT_CODE) {    
    
    /* for all spawn codes */
    for(int i = 0 ; i < cpl->nb_codes ; i++) {     
      MPI_Comm bootcomm = cpl->codes[i]->bootcomm;
      int root = (cpl->prank == 0)?MPI_ROOT:MPI_PROC_NULL;
      
      /* root of local group send to all the remote group */
      MPI_Bcast(&cpl->detached, 1, MPI_INT, root, bootcomm); 
      MPI_Bcast(&cpl->debug, 1, MPI_INT, root, bootcomm); 
      MPI_Bcast(&cpl->nb_codes, 1, MPI_INT, root, bootcomm); 
      MPI_Bcast(&cpl->nb_bindings, 1, MPI_INT, root, bootcomm); 
      MPICPL_PRINT_HIGH(cpl, "SEND DESC TO %s: DETACHED MODE = %d\n", cpl->codes[i]->id, cpl->detached);
      MPICPL_PRINT_HIGH(cpl, "SEND DESC TO %s: DEBUG = %d\n", cpl->codes[i]->id, cpl->debug);
      MPICPL_PRINT_HIGH(cpl, "SEND DESC TO %s: NB CODES = %d\n", cpl->codes[i]->id, cpl->nb_codes);
      MPICPL_PRINT_HIGH(cpl, "SEND DESC TO %s: NB BINDINGS = %d\n", cpl->codes[i]->id, cpl->nb_bindings);        
      
      for(int k = 0 ; k < cpl->nb_codes ; k++) {  
	MPICPL_PRINT_HIGH(cpl, "SENDING DESC OF CODE %s TO %s... (k=%d)\n", 
			  cpl->codes[k]->id, cpl->codes[i]->id, k);
	MPI_Bcast(cpl->codes[k]->id, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
	MPI_Bcast(cpl->codes[k]->program, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
	MPI_Bcast(cpl->codes[k]->cwd, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
	MPI_Bcast(&cpl->codes[k]->np, 1, MPI_INT, root, bootcomm); 
	MPI_Bcast(cpl->codes[k]->args, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
	MPICPL_PRINT_HIGH(cpl, "SEND DESC DONE TO %s: CODE ID = %s, PROGRAM = %s, CWD = %s, NP = %d, ARGS = %s\n", 
			  cpl->codes[i]->id, cpl->codes[k]->id, cpl->codes[k]->program, 
			  cpl->codes[k]->cwd, cpl->codes[k]->np, cpl->codes[k]->args);        
	
      }
      
      for(int k = 0 ; k < cpl->nb_bindings ; k++) { 
	MPI_Bcast(cpl->bindings[k]->id, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
	MPI_Bcast(cpl->bindings[k]->client, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
	MPI_Bcast(cpl->bindings[k]->server, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
	MPICPL_PRINT_HIGH(cpl, "SEND DESC TO %s: BINDING ID = %s, CLIENT = %s, SERVER = %s\n", 
			  cpl->codes[i]->id, cpl->bindings[k]->id, 
			  cpl->bindings[k]->client, cpl->bindings[k]->server);   
      }      
    }
  }
  
  /* from spawn code... (receiver) */
  else {
    int root = 0;
    MPI_Comm bootcomm;
    MPICPL_Get_bootcomm(cpl, &bootcomm); 
    
    /* local group recv from the root of remote group */
    int nb_codes = -1, nb_bindings = -1;
    MPI_Bcast(&cpl->detached, 1, MPI_INT, root, bootcomm); 
    MPI_Bcast(&cpl->debug, 1, MPI_INT, root, bootcomm); 
    MPI_Bcast(&nb_codes, 1, MPI_INT, root, bootcomm); 
    MPI_Bcast(&nb_bindings, 1, MPI_INT, root, bootcomm); 
    MPICPL_PRINT_HIGH(cpl, "RECV DESC: DETACHED = %d\n", cpl->detached);
    MPICPL_PRINT_HIGH(cpl, "RECV DESC: DEBUG = %d\n", cpl->debug);
    MPICPL_PRINT_HIGH(cpl, "RECV DESC: NB CODES = %d\n", nb_codes);
    MPICPL_PRINT_HIGH(cpl, "RECV DESC: NB BINDINGS = %d\n", nb_bindings);        
    
    for(int k = 0 ; k < nb_codes ; k++) {  
      char id[MPICPL_MAX_CHAR], program[MPICPL_MAX_CHAR], cwd[MPICPL_MAX_CHAR]; 
      int np; char args[MPICPL_MAX_CHAR];
      MPI_Bcast(id, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
      MPI_Bcast(program, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
      MPI_Bcast(cwd, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
      MPI_Bcast(&np, 1, MPI_INT, root, bootcomm); 
      MPI_Bcast(args, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
      MPICPL_PRINT_HIGH(cpl, "RECV DESC: CODE ID = %s, PROGRAM = %s, CWD = %s, NP = %d, ARGS = %s\n", 
			id, program, cwd, np, args);        
      /* add code */
      MPICPL_Add_code(cpl, id, program, cwd, np, args);
    }        
    
    for(int k = 0 ; k < nb_bindings ; k++) { 
      char id[MPICPL_MAX_CHAR], client[MPICPL_MAX_CHAR], server[MPICPL_MAX_CHAR]; 
      MPI_Bcast(id, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
      MPI_Bcast(client, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
      MPI_Bcast(server, MPICPL_MAX_CHAR, MPI_CHAR, root, bootcomm); 
      MPICPL_PRINT_HIGH(cpl, "RECV DESC: BINDING ID = %s, CLIENT = %s, SERVER = %s\n", 
			id, client, server);   
      /* add binding */
      MPICPL_Add_binding(cpl, id, client, server); 
    }    
  }
  
  return MPICPL_SUCCESS;  
}

/* ***************************************** */

static
int MPICPL_Remove_binding(MPICPL cpl, 
			  char* binding_id)
{
  assert(cpl != NULL);
  MPICPL_PRINT_HIGH(cpl, "REMOVE BINDING %s\n", binding_id);
  int root = 0;
  int err;
  int found = -1;
  MPICPL_Binding binding, tmp;
  
  /* find binding */
  for(int i = 0 ; i < cpl->nb_bindings ; i++) {
    if(strcmp(binding_id, cpl->bindings[i]->id) == 0) {
      found = i;
      break;
    }
  }
  assert(found != -1);
  binding = cpl->bindings[found];
  
  /* disconnect */
  if(cpl->kind != MPICPL_BOOT_CODE && binding->intercomm != MPI_COMM_NULL) {
    err = MPI_Comm_disconnect(&binding->intercomm);
    assert(err == MPI_SUCCESS);
  }
  
  /* close port  */
  if(cpl->kind != MPICPL_BOOT_CODE && cpl->prank == root && strcmp(cpl->id, binding->server) == 0) {        
    MPICPL_PRINT_HIGH(cpl, "CLOSING PORT %s\n", binding->portname);    
    err = MPI_Close_port(binding->portname);
    assert(err == MPI_SUCCESS);
    
  }
  
  /* free memory */
  if(binding->intercomm != MPI_COMM_NULL)
    MPI_Comm_free(&binding->intercomm);
  free(binding);
  tmp = cpl->bindings[cpl->nb_bindings-1];
  cpl->bindings[found] = tmp;
  cpl->nb_bindings--;
  cpl->bindings = (MPICPL_Binding*)realloc(cpl->bindings, cpl->nb_bindings*sizeof(MPICPL_Binding));
  
  return MPICPL_SUCCESS;
} 

/* ***************************************** */

static
int MPICPL_Remove_code(MPICPL cpl, 
		       char* code_id)
{
  assert(cpl != NULL);
  MPICPL_PRINT_HIGH(cpl, "REMOVE CODE %s\n", code_id);
  int found = -1;
  MPICPL_Code code, tmp;
  
  /* find code */
  for(int i = 0 ; i < cpl->nb_codes ; i++) {
    if(strcmp(code_id, cpl->codes[i]->id) == 0) {
      found = i;
      break;
    }
  }
  assert(found != -1);
  code = cpl->codes[found];
  
  /* remove all bindings with this code */
  int i = 0;
  while(i <  cpl->nb_bindings)  {
    if(strcmp(code_id, cpl->bindings[i]->client) == 0
       || strcmp(code_id, cpl->bindings[i]->server) == 0)
      MPICPL_Remove_binding(cpl, cpl->bindings[i]->id);
    else 
      i++;
  }
  
  /* free memory */
  if(code->bootcomm != MPI_COMM_NULL)
    MPI_Comm_free(&code->bootcomm);
  free(code);
  tmp = cpl->codes[cpl->nb_codes-1];
  cpl->codes[found] = tmp;
  cpl->nb_codes--;
  cpl->codes = (MPICPL_Code*)realloc(cpl->codes, cpl->nb_codes*sizeof(MPICPL_Code));
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

static
int MPICPL_Remove_universe(MPICPL cpl)
{
  assert(cpl != NULL);
  MPICPL_PRINT_HIGH(cpl, "REMOVE UNIVERSE\n");
  int root = 0;
  int err;
  
  /* disconnect */
  if(cpl->kind != MPICPL_BOOT_CODE && cpl->univcomm != MPI_COMM_NULL) {
    err = MPI_Comm_disconnect(&cpl->univcomm);
    assert(err == MPI_SUCCESS);
  }
  
  /* close port */
  if(cpl->kind != MPICPL_BOOT_CODE && cpl->prank == root && strcmp(cpl->id, cpl->codes[0]->id) == 0) {
    
    for(int i = 1 ; i < cpl->nb_codes ; i++) {         
      char binding[MPICPL_MAX_CHAR];
      sprintf(binding,"UNIVERSE-%d",i-1);
      
      MPICPL_PRINT_HIGH(cpl, "CLOSING PORT %s\n", cpl->codes[i]->univ_portname);
      err = MPI_Close_port(cpl->codes[i]->univ_portname);
      assert(err == MPI_SUCCESS);
      
    }
    
  }
  return MPICPL_SUCCESS;
}

/* ***************************************** */

static
int MPICPL_Remove_all(MPICPL cpl)
{
  assert(cpl != NULL);
  MPICPL_PRINT_HIGH(cpl, "REMOVE ALL\n");
  
  /* remove universe */
  MPICPL_Remove_universe(cpl);
  
  /* remove all codes (and bindings) */
  int i = 0;
  while(cpl->nb_codes > 0) 
    MPICPL_Remove_code(cpl, cpl->codes[i]->id);
  
  assert(cpl->nb_codes == 0);
  assert(cpl->nb_bindings == 0);
  cpl->bindings = NULL;
  cpl->codes = NULL;
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */
/*          REQUEST MANAGEMENT               */
/* ***************************************** */

static
int MPICPL_Send_request(MPICPL cpl, int request, int collective)
{  
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE && !cpl->detached);
  assert(request >= 0 && request < NB_REQUESTS);
  int root = 0;
  
  /* code rank */
  int crank = -1;
  MPICPL_Find_code(cpl, cpl->id, NULL, &crank);
  assert(crank != -1);
  
  /* send request */
  if(cpl->prank == root) 
    if((collective && crank == 0) || !collective) {
      MPICPL_PRINT_HIGH(cpl, "SEND REQUEST %s (COLLECTIVE = %d) \n", REQUESTS[request], collective);  
      MPI_Send(&collective, 1, MPI_INT, root, MPICPL_TAG+1, cpl->parentcomm); /* different tag useful ? */
      MPI_Send(&request, 1, MPI_INT, root, MPICPL_TAG, cpl->parentcomm);    
    }
  
  MPI_Barrier(cpl->worldcomm); /* useful ? */
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

static
int MPICPL_Recv_request(MPICPL cpl, int * request, int * crank) /* return -1 if collective else code rank */
{  
  assert(cpl != NULL && cpl->kind == MPICPL_BOOT_CODE && !cpl->detached);
  assert(request != NULL);
  assert(crank != NULL);
  
  int nb_codes = cpl->nb_codes;
  MPI_Request mpireqs[nb_codes];
  // MPI_Status statuses[nb_codes];
  // int req[nb_codes];
  int root = 0;
  MPI_Status status;
  
  MPICPL_PRINT_HIGH(cpl, "RECV REQUEST...\n");
  
  /* recv if request is collective or not ? */
  int collective = -1;
  *crank = -1; /* code rank for non-collective request (or 1 if collective) */
  int index = -1;
  if(cpl->prank == root) {
    for(int i = 0 ; i < cpl->nb_codes ; i++)
      MPI_Irecv(&collective, 1, MPI_INT, root, MPICPL_TAG+1, cpl->codes[i]->bootcomm, &mpireqs[i]); /* different tag useful ? */
    // MPICPL_PRINT_HIGH(cpl, "WAIT ANY...\n");
    int err = MPI_Waitany(cpl->nb_codes, mpireqs, &index, &status);
    assert(err == MPI_SUCCESS);
    if(collective) *crank = -1; else *crank = index;
    // MPICPL_PRINT_HIGH(cpl, "RECV REQUEST (COLLECTIVE = %d, CODE RANK = %d)\n", collective, *crank);
    for(int i = 0 ; i < cpl->nb_codes ; i++) if(mpireqs[i] != MPI_REQUEST_NULL) MPI_Cancel(&mpireqs[i]);
    MPI_Waitall(cpl->nb_codes, mpireqs, MPI_STATUSES_IGNORE); /* free request */
  }
  
  /* broadcast collective/crank on local comm. */
  MPI_Bcast(&collective, 1, MPI_INT, root, cpl->worldcomm); 
  MPI_Bcast(crank, 1, MPI_INT, root, cpl->worldcomm); 
  assert(*crank >= -1);
  
  /* recv request (collective or non-collective case) */
  if(cpl->prank == root) {
    MPI_Recv(request, 1, MPI_INT, root, MPICPL_TAG, cpl->codes[index]->bootcomm, &status);
  } 
  
  /* broadcast request on local comm. */
  MPI_Bcast(request, 1, MPI_INT, root, cpl->worldcomm); 
  
  MPICPL_PRINT_HIGH(cpl, "RECV REQUEST %s (COLLECTIVE = %d, CODE RANK = %d)\n", REQUESTS[*request], collective, *crank);  
  assert(*request >= 0 && *request < NB_REQUESTS);
  
  return MPICPL_SUCCESS;
}


/* ***************************************** */

static
int MPICPL_Send_message(MPICPL cpl, char * message)
{  
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE);
  assert(message != NULL);
  int root = 0;
  
  if(cpl->prank == root) {
    MPICPL_PRINT_HIGH(cpl, "SEND MESSAGE \"%s\"\n", message);  
    MPI_Send(message, MPICPL_MAX_MSG_SIZE, MPI_CHAR, root, MPICPL_TAG, cpl->parentcomm);    
  }
  MPI_Barrier(cpl->worldcomm); /* useful ? */   
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

static
int MPICPL_Recv_message(MPICPL cpl, char * message, int crank)
{  
  assert(cpl != NULL && cpl->kind == MPICPL_BOOT_CODE && !cpl->detached);
  assert(message != NULL);
  
  int root = 0;
  
  /* wait all messages (one per spawn code) */
  if(cpl->prank == root) {
    MPI_Recv(message, MPICPL_MAX_MSG_SIZE, MPI_CHAR, root, MPICPL_TAG, cpl->codes[crank]->bootcomm, MPI_STATUS_IGNORE); 
  }  
  
  /* broadcast message on local comm. */
  MPI_Bcast(message, MPICPL_MAX_MSG_SIZE, MPI_CHAR, root, cpl->worldcomm); 
  
  if(cpl->prank == root)
    MPICPL_PRINT_HIGH(cpl, "RECV MESSAGE \"%s\"\n", message);
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */
/*        PUBLIC FOR SPAWN CODE              */
/* ***************************************** */

int MPICPL_Init(MPICPL* cpl, 
		MPI_Comm worldcomm,
		const char * id)
{
  *cpl = (MPICPL)malloc(sizeof(struct MPICPL_t));
  (*cpl)->worldcomm = worldcomm;
  MPI_Comm_rank(worldcomm, &(*cpl)->prank);
  MPI_Comm_size(worldcomm, &(*cpl)->psize);
  strcpy((*cpl)->id, id);
  (*cpl)->kind = MPICPL_SPAWN_CODE;
  (*cpl)->nb_codes = 0;
  (*cpl)->nb_bindings = 0;
  (*cpl)->codes = NULL;
  (*cpl)->bindings = NULL;
  
  /* misc. */
  (*cpl)->debug = MPICPL_DEBUG_NO; /* default */
  (*cpl)->parentcomm = MPI_COMM_NULL;
  MPI_Comm_get_parent(&(*cpl)->parentcomm);    
  (*cpl)->univcomm = MPI_COMM_NULL;
  
  /* debug */
  MPICPL_PRINT_LOW(*cpl, "INIT SPAWN CODE\n"); 
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Run(MPICPL cpl)
{  
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE);  
  
  /* synchronization with all spawn codes */
  // MPICPL_Synchronize_internal(cpl);
  
  /* broadcast the coupling desc */
  MPICPL_PRINT_LOW(cpl, "BROADCAST DESC\n"); 
  MPICPL_Bcast_desc(cpl);
  
  /* get current code */
  MPICPL_Code code; 
  MPICPL_Find_code(cpl, cpl->id, &code, NULL);
  
  /* check spawn code ID */  
  if(code == NULL) {
    MPICPL_PRINT_ERROR(cpl, "BAD CODE ID <%s>\n", cpl->id);
    MPI_Abort(cpl->worldcomm, 1); 
    return MPICPL_ERROR;
  }
  
  /* check cwd */
  char cwd[MPICPL_MAX_CHAR];
  char * _cwd = getcwd(cwd, MPICPL_MAX_CHAR); /* check */
  assert(_cwd != NULL);
  MPICPL_PRINT_LOW(cpl, "CURRENT WORKING DIRECTORY = %s\n", cwd);
  struct stat statcwd, statcwd0;
  stat(cwd, &statcwd);
  stat(code->cwd, &statcwd0);
  if(statcwd.st_dev != statcwd0.st_dev || statcwd.st_ino != statcwd0.st_ino) {
    MPICPL_PRINT_ERROR(cpl, "ERROR: BAD CWD %s FOR SPAWN CODE %s (%s)\n",
		       code->cwd, code->id, cwd);
    MPI_Abort(cpl->worldcomm, 1); 
    return MPICPL_ERROR;
  }
    
  /* create universe intra-communicator */
  MPICPL_PRINT_LOW(cpl, "INTERCONNECT UNIVERSE\n");
  MPICPL_Interconnect_universe(cpl, &cpl->univcomm);

  /* create universe intra-communicator */
  MPICPL_PRINT_LOW(cpl, "BARRIER on UNIVERSE\n");
  MPI_Barrier(cpl->univcomm);
  
  /* synchronization with all spawn codes */
  MPICPL_Synchronize_internal(cpl);
  
  /* send requests to create bindings between codes */
  for(int i = 0 ; i < cpl->nb_bindings ; i++)  
    MPICPL_Interconnect_codes(cpl, cpl->bindings[i]);    
  
  MPICPL_PRINT_LOW(cpl, "RUN...\n");
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Finalize(MPICPL cpl)
{
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE);
  
  /* debug */
  MPICPL_PRINT_LOW(cpl, "FINALIZE SPAWN CODE\n"); 
  
  /* send request END to boot code... */
  if(!cpl->detached)
    MPICPL_Send_request(cpl, MPICPL_REQUEST_END, 1);
  
  /* free memory */
  MPICPL_Remove_all(cpl);
  
  free(cpl);
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Get_intercomm(MPICPL cpl, 
			 char* binding_id, 
			 MPI_Comm* intercomm)
{
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE);  
  *intercomm = MPI_COMM_NULL;
  MPICPL_Binding binding; 
  MPICPL_Find_binding(cpl, binding_id, &binding, NULL);
  if(binding == NULL) return MPICPL_ERROR;
  *intercomm = binding->intercomm;    
  if(*intercomm == MPI_COMM_NULL) return MPICPL_ERROR;
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Get_univcomm(MPICPL cpl, 
			MPI_Comm* univcomm)
{
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE);  
  MPI_Comm_dup(cpl->univcomm, univcomm);   
  return MPICPL_SUCCESS; 
}

/* ***************************************** */

int MPICPL_Print(MPICPL cpl)
{
  assert(cpl != NULL);
  
  printf("[%s:%d] id = %s\n", cpl->id, cpl->prank, cpl->id);
  printf("[%s:%d] boot code = %d\n", cpl->id, cpl->prank, cpl->kind == MPICPL_BOOT_CODE);
  printf("[%s:%d] nb codes = %d\n", cpl->id, cpl->prank, cpl->nb_codes);
  printf("[%s:%d] nb bindings = %d\n", cpl->id, cpl->prank, cpl->nb_bindings);
  for(int i = 0 ; i < cpl->nb_codes ; i++) {
    printf("[%s:%d] code %s, program = %s, np = %d, args = %s\n", 	   
	   cpl->id, cpl->prank, 
	   cpl->codes[i]->id, 
	   cpl->codes[i]->program, 
	   cpl->codes[i]->np, 
	   cpl->codes[i]->args);
  }
  for(int i = 0 ; i < cpl->nb_bindings ; i++) {
    printf("[%s:%d] binding %s, client = %s, server = %s\n", 
	   cpl->id, cpl->prank, 
	   cpl->bindings[i]->id, 
	   cpl->bindings[i]->client, 
	   cpl->bindings[i]->server);
  }
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Restart(MPICPL cpl, 
		   char* new_program, 
		   char* new_cwd, 
		   int new_np, 
		   char* new_argv[])
{
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE && !cpl->detached);   
  
  /* check args */
  assert(new_program != NULL);
  assert(new_cwd != NULL);
  assert(new_np > 0);
  assert(new_argv != NULL);
  
  /* send request RESTART to boot code... */
  MPICPL_Send_request(cpl, MPICPL_REQUEST_RESTART, 1);        
  
  /* send restart parameters */
  int root = (cpl->prank == 0)?MPI_ROOT:MPI_PROC_NULL;
  MPI_Bcast(new_program, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->parentcomm); 
  MPI_Bcast(new_cwd, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->parentcomm); 
  MPI_Bcast(&new_np, 1, MPI_INT, root, cpl->parentcomm); 
  
  /* args */
  char *argz; size_t argz_len;
  argz_create(new_argv, &argz, &argz_len);
  argz_stringify(argz, argz_len, ' ');
  char new_args[MPICPL_MAX_CHAR];
  strcpy(new_args, argz);
  free(argz);
  MPICPL_PRINT_HIGH(cpl, "SEND RESTART ARGS: PROGRAM = %s, CWD = %s, NP = %d, ARGS = %s\n", 
		    new_program, new_cwd, new_np, new_args);   
  MPI_Bcast(new_args, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->parentcomm); 
  
  /* finalize spawn code */
  MPICPL_Remove_all(cpl);
  free(cpl);
  
  MPICPL_PRINT_LOW(cpl, "STOP SPAWN CODES BEFORE RESTART...\n"); 
  MPI_Finalize();
  exit(EXIT_SUCCESS); // terminate process here!
  
  return MPICPL_SUCCESS;  
}

/* ***************************************** */

int MPICPL_Message(MPICPL cpl, 
		   char* message)
{
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE && !cpl->detached);   
  assert(message != NULL);
  assert(strlen(message) < MPICPL_MAX_MSG_SIZE);
  
  /* send request MESSAGE to boot code... */
  MPICPL_Send_request(cpl, MPICPL_REQUEST_MESSAGE, 0); /* non-collective */
  
  MPICPL_PRINT_LOW(cpl, "SEND MESSAGE %s\n", message);
  
  /* send message */
  MPICPL_Send_message(cpl,message);
  
  return MPICPL_SUCCESS;   
}

/* ***************************************** */

int MPICPL_Synchronize(MPICPL cpl)
{
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE && !cpl->detached);   
  
  /* send request RESTART to boot code... */
  MPICPL_Send_request(cpl, MPICPL_REQUEST_SYNCHRONIZE, 1);
  
  /* flush standard outputs */
  fflush(stdout); fflush(stderr);
  
  /* synchronization of all code with barrier */
  MPICPL_Synchronize_internal(cpl);
  
  return MPICPL_SUCCESS;  
}

/* ***************************************** */

int MPICPL_Get_bootcomm(MPICPL cpl,
			MPI_Comm* bootcomm)
{ 
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE);
  *bootcomm = cpl->parentcomm;
  assert(*bootcomm != MPI_COMM_NULL);
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Barrier(MPICPL cpl)
{
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE);
  assert(cpl->univcomm != MPI_COMM_NULL);
  MPICPL_PRINT_LOW(cpl, "BARRIER on UNIVERSE\n");
  MPI_Barrier(cpl->univcomm);
  return MPICPL_SUCCESS;
}


/* ***************************************** */
/*         Dynamic Code Management           */
/* ***************************************** */

int MPICPL_Accept(MPICPL cpl, int bootport, MPI_Comm * intercomm) 
{ 
  assert(cpl != NULL && cpl->kind != MPICPL_BOOT_CODE && bootport > 0 && !cpl->detached);   
  
  int err;
  // int fd;
  
  MPICPL_PRINT_LOW(cpl, "DYNAMIC CONNECTION MANAGEMENT: SERVER ACCEPTING... (bootport=%d)\n", bootport); 
  
  // 0) send listen request to boot code
  MPICPL_Send_request(cpl, MPICPL_REQUEST_LISTEN, 0); /* non-collective */

  // 1)  send bootport to boot code via MPI
  if(cpl->prank == 0) {
    int root = 0;
    MPI_Send(&bootport, 1, MPI_INT, root, MPICPL_TAG, cpl->parentcomm); 
  }
  
  // 2) MPI open port
  char portname[MPI_MAX_PORT_NAME];
  if(cpl->prank == 0) {
    err = MPI_Open_port(MPI_INFO_NULL, portname); 
    assert(err == MPI_SUCCESS);
  }  
  
  // 3)  send MPI portname to boot code via MPI
  if(cpl->prank == 0) {
    int root = 0;
    MPI_Send(portname, MPI_MAX_PORT_NAME, MPI_CHAR, root, MPICPL_TAG, cpl->parentcomm); 
  }
    
  // 4) MPI comm accept from external client
  err = MPI_Comm_accept(portname,   /* in */
			MPI_INFO_NULL,
			0,           /* root */
			cpl->worldcomm,
			intercomm);  /* out */
  assert(err == MPI_SUCCESS);
  
  MPICPL_PRINT_LOW(cpl, "DYNAMIC CONNECTION MANAGEMENT: SERVER ACCEPT DONE!\n");
  
  return MPICPL_SUCCESS; 
} 

/* ***************************************** */

int MPICPL_Connect(MPI_Comm world, char * boothostname, int bootport, MPI_Comm * intercomm)
{ 
  int err;
  int fd;
  int prank;
  MPI_Comm_rank(MPI_COMM_WORLD, &prank); 
  char portname[MPI_MAX_PORT_NAME];
  
  printf("[CLIENT:%d] DYNAMIC CONNECTION MANAGEMENT: CLIENT CONNECTING... (boot=%s:%d)\n", prank, boothostname, bootport); 
  
  // 1) connect socket boothostname:bootport
  if(prank == 0) {
    fd = socket(AF_INET,SOCK_STREAM,0);
    assert(fd >= 0);
    struct hostent * server = gethostbyname(boothostname);
    assert(server != NULL);
    struct sockaddr_in addr;
    addr.sin_family      = AF_INET;
    addr.sin_port        = htons(bootport);                          /* n� port (16 bits) */
    // addr.sin_addr.s_addr = *(unsigned long *)server->h_addr;      /* addr IP (32 bits) */
    addr.sin_addr.s_addr = *(unsigned long *)server->h_addr_list[0]; /* addr IP (32 bits) */
    err = connect(fd,(struct sockaddr *)&addr, sizeof(addr));
    assert(err == 0);
  }
  
  // 2) recv MPI portname from boot via socket
  if(prank == 0) {
    int r = read(fd, portname, MPI_MAX_PORT_NAME*sizeof(char));    
    assert(r ==  MPI_MAX_PORT_NAME);
    close(fd);
  }
  
  // 3) MPI comm connect 
  err = MPI_Comm_connect(portname,      /* used only on root */
			 MPI_INFO_NULL,
			 0,              /* root */
			 world,          /* communicator */
			 intercomm);
  assert(err == MPI_SUCCESS);
  
  printf("[CLIENT:%d] DYNAMIC CONNECTION MANAGEMENT: CLIENT CONNECT DONE!\n", prank);
  
  return MPICPL_SUCCESS; 
} 

/* ***************************************** */

typedef struct {
  MPICPL cpl;
  int crank;  /* code rank */
} socklisten_t;

static void* socklisten(void* arg)
{  
  socklisten_t * s = (socklisten_t*)arg;
  int crank = s->crank;
  MPICPL cpl = s->cpl;
  assert(cpl != NULL && cpl->kind == MPICPL_BOOT_CODE && cpl->prank == 0);  
  assert(crank >= 0);
  
  int root = 0;
  MPI_Status status;

  /* get bootport */
  int bootport = -1;
  MPI_Recv(&bootport, 1, MPI_INT, root, MPICPL_TAG, cpl->codes[crank]->bootcomm, &status); 
  MPICPL_PRINT_HIGH(cpl, "DYNAMIC CONNECTION MANAGEMENT: RECV BOOT PORT FROM SERVER (%d)\n", bootport);  
  assert(bootport > 0);

  /* get port name */
  char portname[MPI_MAX_PORT_NAME]; /* from server code */
  MPI_Recv(portname, MPI_MAX_PORT_NAME, MPI_CHAR, root, MPICPL_TAG, cpl->codes[crank]->bootcomm, &status); 
  MPICPL_PRINT_HIGH(cpl, "DYNAMIC CONNECTION MANAGEMENT: RECV PORT NAME FROM SERVER (%s)\n", portname);  
  
  /* get hostname */
  int hostnamelen; char hostname[MPI_MAX_PROCESSOR_NAME]; 
  MPI_Get_processor_name(hostname, &hostnamelen); /* get MPI hostname */
  
  /* server socket listen on all IP addresses */
  int fd = socket(AF_INET,SOCK_STREAM,0);
  assert(fd >= 0);
  struct sockaddr_in addr;
  addr.sin_family      = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  addr.sin_port        = htons(bootport);
  int err = bind(fd,(struct sockaddr*)&addr, sizeof(addr));
  assert(err >= 0);
  listen(fd, 4);
  MPICPL_PRINT_LOW(cpl, "DYNAMIC CONNECTION MANAGEMENT: SOCKET LISTEN ON %s:%d\n", hostname, bootport);
  assert(ntohs(addr.sin_port) == bootport);
  
  /* accept socket connection from client code  */
  int fdc = accept(fd, NULL, NULL);
  assert(fdc > 0);
  int r = write(fdc, portname, MPI_MAX_PORT_NAME*sizeof(char));
  assert(r == MPI_MAX_PORT_NAME);
  MPICPL_PRINT_HIGH(cpl, "DYNAMIC CONNECTION MANAGEMENT: SEND PORT NAME TO CLIENT (%s)\n", portname);
  close(fdc);
  
  /* end */
  close(fd);

  /* free mem */
  free(s);
  
  return NULL;
}

/* ***************************************** */
/*         PUBLIC FOR BOOT CODE              */
/* ***************************************** */

int MPICPL_Init_boot(MPICPL* cpl, 
		     MPI_Comm worldcomm,
		     const char * id,
		     // int port,
		     int detached,
		     int debug)
{
  *cpl = (MPICPL)malloc(sizeof(struct MPICPL_t));
  (*cpl)->worldcomm = worldcomm;
  MPI_Comm_rank(worldcomm, &(*cpl)->prank);
  MPI_Comm_size(worldcomm, &(*cpl)->psize);
  strcpy((*cpl)->id, id);
  (*cpl)->kind = MPICPL_BOOT_CODE;
  (*cpl)->nb_codes = 0;
  (*cpl)->nb_bindings = 0;
  (*cpl)->codes = NULL;
  (*cpl)->bindings = NULL;
  
  /* misc. */
  (*cpl)->detached = detached;
  (*cpl)->debug = debug;
  (*cpl)->parentcomm = MPI_COMM_NULL;
  MPI_Comm_get_parent(&(*cpl)->parentcomm);    
  (*cpl)->univcomm = MPI_COMM_NULL;
  
  /* debug */
  MPICPL_PRINT_LOW(*cpl, "INIT BOOT CODE\n"); 
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Add_code(MPICPL cpl, 
		    const char* code_id, 
		    const char* program, 
		    const char* cwd, 
		    int np, 
		    const char* args)
{
  assert(cpl != NULL);
  MPICPL_PRINT_HIGH(cpl, "ADD CODE %s\n", code_id);
  cpl->nb_codes++;
  cpl->codes = (MPICPL_Code*)realloc(cpl->codes, cpl->nb_codes*sizeof(MPICPL_Code));
  assert(cpl->codes != NULL);
  MPICPL_Code code = (MPICPL_Code)malloc(sizeof(struct MPICPL_Code_t));
  assert(code != NULL);
  strcpy(code->id, code_id);
  
  /* set program as an absolute path */
  char cwd0[MPICPL_MAX_CHAR];
  char * _cwd = getcwd(cwd0, MPICPL_MAX_CHAR);
  assert(_cwd != NULL);
  if(program[0] == '/')
    strcpy(code->program, program);
  else
    sprintf(code->program,"%s/%s",cwd0, program);
  
  /* set current working directory as an absolute path */
  if(cwd != NULL && cwd[0] == '/')
    strcpy(code->cwd, cwd);
  else if(cwd != NULL && cwd[0] == '.') 
    sprintf(code->cwd,"%s/%s",cwd0, cwd);
  else 
    strcpy(code->cwd, cwd0);
  
  /* np */
  code->np = np;
  
  /* args */
  code->args[0] = 0;
  if(args != NULL) strcpy(code->args, args); /* ??? */
  
  code->bootcomm = MPI_COMM_NULL;
  cpl->codes[cpl->nb_codes-1] = code;  
  
  MPICPL_PRINT_HIGH(cpl, "ADD CODE %s DONE!\n", code_id);
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Add_binding(MPICPL cpl, 
		       const char* binding_id, 
		       const char* client, 
		       const char* server)
{
  assert(cpl != NULL);
  MPICPL_PRINT_HIGH(cpl, "ADD BINDING %s\n", binding_id);
  cpl->nb_bindings++;
  cpl->bindings = (MPICPL_Binding*)realloc(cpl->bindings, cpl->nb_bindings*sizeof(MPICPL_Binding));
  assert(cpl->bindings != NULL);
  MPICPL_Binding binding = (MPICPL_Binding)malloc(sizeof(struct MPICPL_Binding_t));
  assert(binding != NULL);
  strcpy(binding->id, binding_id);
  strcpy(binding->client, client);
  strcpy(binding->server, server);
  binding->intercomm = MPI_COMM_NULL;
  cpl->bindings[cpl->nb_bindings-1] = binding;  
  
  MPICPL_PRINT_HIGH(cpl, "ADD BINDING %s DONE!\n", binding_id);
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Run_boot(MPICPL cpl)
{  
  assert(cpl != NULL && cpl->kind == MPICPL_BOOT_CODE);  
  
  /* spawn all codes */
  for(int i = 0 ; i < cpl->nb_codes ; i++) 
    MPICPL_Spawn_code(cpl, cpl->codes[i]);
  
  /* synchronization with all spawn codes */
  // MPICPL_Synchronize_internal(cpl);
  
  /* broadcast the coupling desc */
  MPICPL_Bcast_desc(cpl);
    
    /* create universe intra-communicator */
  MPICPL_PRINT_LOW(cpl, "INTERCONNECT UNIVERSE\n");
  MPICPL_Interconnect_universe(cpl, &cpl->univcomm); 
  
  /* synchronization with all spawn codes */
  MPICPL_Synchronize_internal(cpl);
  
  /* send requests to create bindings between codes */
  for(int i = 0 ; i < cpl->nb_bindings ; i++)  
    MPICPL_Interconnect_codes(cpl, cpl->bindings[i]);

  MPICPL_PRINT_LOW(cpl, "RUN...\n");
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Finalize_boot(MPICPL cpl)
{
  assert(cpl != NULL && cpl->kind == MPICPL_BOOT_CODE);
  
  /* debug */
  MPICPL_PRINT_LOW(cpl, "FINALIZE BOOT CODE\n"); 
  
  /* free memory */
  MPICPL_Remove_all(cpl);
  
  free(cpl);
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Wait(MPICPL cpl, int * request, char * message)
{  
  assert(cpl != NULL && cpl->kind == MPICPL_BOOT_CODE && !cpl->detached); 
  
  
  /* reset message */
  if(message != NULL) message[0] = 0;
  
  /* recv... */
  MPICPL_PRINT_HIGH(cpl, "WAITING FOR REQUEST...\n");  
  int crank = -1;
  MPICPL_Recv_request(cpl, request, &crank);
  assert(crank >= -1); /* check */
  
  
  /* end  (collective request) */
  if(*request == MPICPL_REQUEST_END) {
    MPICPL_PRINT_LOW(cpl, "END OF ALL SPAWN CODES\n");
    assert(crank == -1);
    MPICPL_Remove_all(cpl);
  }
  
  /* restart (collective request) */
  else if(*request == MPICPL_REQUEST_RESTART) {
    MPICPL_PRINT_LOW(cpl, "RESTART ALL SPAWN CODES\n");
    assert(crank == -1);
    
    /* recv restart args */
    for(int i = 0 ; i < cpl->nb_codes ; i++) {
      int root = 0;      
      MPI_Bcast(cpl->codes[i]->program, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->codes[i]->bootcomm); 
      MPI_Bcast(cpl->codes[i]->cwd, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->codes[i]->bootcomm); 
      MPI_Bcast(&cpl->codes[i]->np, 1, MPI_INT, root, cpl->codes[i]->bootcomm); 
      MPI_Bcast(cpl->codes[i]->args, MPICPL_MAX_CHAR, MPI_CHAR, root, cpl->codes[i]->bootcomm); 
      MPICPL_PRINT_HIGH(cpl, "RECV RESTART ARGS: CODE ID = %s, PROGRAM = %s, CWD = %s, NP = %d, ARGS = %s\n", 
			cpl->codes[i]->id, cpl->codes[i]->program, cpl->codes[i]->cwd,
			cpl->codes[i]->np, cpl->codes[i]->args);              
    }  
    
    /* restart (re-spawn with new args) */
    MPICPL_Run_boot(cpl);
    
  }
  
  /* synchronize (collective request) */
  else if(*request == MPICPL_REQUEST_SYNCHRONIZE) {
    MPICPL_PRINT_LOW(cpl, "SYNCHRONIZE ALL CODES\n");
    assert(crank == -1);
    fflush(stdout); fflush(stderr);
    MPICPL_Synchronize_internal(cpl);    
  }
  
  /* message (non-collective request) */
  else if(*request == MPICPL_REQUEST_MESSAGE) {
    assert(crank != -1);
    char msg[MPICPL_MAX_MSG_SIZE];
    MPICPL_Recv_message(cpl, msg, crank);
    MPICPL_PRINT_LOW(cpl, "RECV MESSAGE \"%s\" FROM CODE %s\n", msg, cpl->codes[crank]->id);
    if(message != NULL) strcpy(message, msg);
  }
  
  /* listen for dynamic connection (non-collective request) */
  else if(*request == MPICPL_REQUEST_LISTEN) {
    MPICPL_PRINT_LOW(cpl, "RECV LISTEN REQUEST !!!\n");
    assert(crank != -1);
    if(cpl->prank == 0) {

      socklisten_t * s = malloc(sizeof(socklisten_t));
      s->crank = crank;
      s->cpl = cpl;

      /* start socket server in a thread (dynamic connection management) */
      pthread_t tid;
      pthread_create(&tid, NULL, socklisten, (void*)s);
    }
  }
  
  /* unknown request */
  else {
    MPICPL_PRINT_LOW(cpl, "RECV UNKNOWN REQUEST !!!\n");
  }
  
  /* synchronize boot code */
  MPI_Barrier(cpl->worldcomm);
  
  return MPICPL_SUCCESS;
}

/* ***************************************** */

int MPICPL_Get_spawncomm(MPICPL cpl,
			 char* code_id, 
			 MPI_Comm* spawncomm)
{ 
  assert(cpl != NULL && cpl->kind == MPICPL_BOOT_CODE);  
  *spawncomm = MPI_COMM_NULL;
  MPICPL_Code code; 
  MPICPL_Find_code(cpl, code_id, &code, NULL);
  if(code == NULL) return MPICPL_ERROR;
  *spawncomm = code->bootcomm;  
  if(*spawncomm == MPI_COMM_NULL) return MPICPL_ERROR; 
  return MPICPL_SUCCESS;
}

/* ***************************************** */


