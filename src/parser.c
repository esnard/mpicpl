/** 
 * @file  parser.c
 * @brief MPICPL XML Parser
 * @author Aurelien Esnard                     
 */

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include "myargz.h"
#include "struct.h"
#include "mydebug.h"
#include "mpicplb.h"

/* ***************************************** */

static char* find(char* name, int argc, char** xmlargn, char** xmlargv)
{
  for(int i = 0 ; i < argc ; i++)
    if(strcmp(name, xmlargn[i]) == 0)
      return xmlargv[i];
  return NULL;
}

/* ***************************************** */

static char* getProperty(xmlNodePtr node, char* property,
			 int argc, char** xmlargn, char** xmlargv)
{
  assert(node && property);
  char * value = xmlGetProp(node, (xmlChar*)property);
  if(!value) return NULL;
  assert(strlen(value) < MPICPL_MAX_CHAR);


  /* perform replacement if ${NAME} is found */
  char * newvalue = malloc(MPICPL_MAX_CHAR*sizeof(char));  
  newvalue[0] = 0;
  char * v = value;
  char * vv;
  while((vv = index(v,'$')) != NULL) {
    char* vs = index(vv,'{'); assert(vs);
    char* ve = index(vv,'}'); assert(ve);
    int prefixlen = vv - v;    
    strncat(newvalue, v, prefixlen);    
    int len = ve -(vs+1);
    char name[len+1]; 
    strncpy(name, vs+1, len); name[len] = 0;
    char * replace = find(name, argc, xmlargn, xmlargv);
    assert(replace);
    strcat(newvalue, replace);    
    v = ve+1;
  }
  strcat(newvalue, v);/* suffix */
  assert(strlen(newvalue) < MPICPL_MAX_CHAR);

  // if(v != value)
  // printf("replace %s by %s !\n", value, newvalue);
  free(value);
  return newvalue;
}

/* ***************************************** */

static void parse(MPICPL cpl, xmlNodePtr current_node,
		  int argc, char** xmlargn, char** xmlargv)
{
  /* parse task attributes */
  char* elname = (char*)current_node->name;

  if(strcasecmp(elname, "universe") == 0) {
    char * id =  getProperty(current_node, "id", argc, xmlargn, xmlargv);
    char * args = getProperty(current_node, "args", argc, xmlargn, xmlargv);
  }
  // code element
  else if(strcasecmp(elname, "code") == 0) {
    char * id = getProperty(current_node, "id", argc, xmlargn, xmlargv);
    char * args = getProperty(current_node, "args", argc, xmlargn, xmlargv);
    char * program = getProperty(current_node, "program", argc, xmlargn, xmlargv);
    char * np = getProperty(current_node, "np", argc, xmlargn, xmlargv);
    char * cwd = getProperty(current_node, "cwd", argc, xmlargn, xmlargv);
    
    /* add code */
    MPICPL_Add_code(cpl, id, program, cwd, atoi(np), args);
    
    free(np);
    free(id);
    free(program);
    free(cwd);
    free(args);
  }
  // binding element
  else if(strcasecmp(elname, "binding") == 0) {
    char* id = getProperty(current_node, "id", argc, xmlargn, xmlargv);
    char* client = getProperty(current_node, "client", argc, xmlargn, xmlargv);
    char* server = getProperty(current_node, "server", argc, xmlargn, xmlargv);
    
    /* add binding */
    MPICPL_Add_binding(cpl, id, client, server); 
    
    free(id);
    free(client);
    free(server);
  }
  else if(strcasecmp(elname, "codes") == 0) {
    /* skip */
  }
  else if(strcasecmp(elname, "bindings") == 0) {
    /* skip */
  }
  // standard text/comment tag
  else if(strcasecmp(elname, "text") == 0 || strcasecmp(elname, "comment") == 0) {
    /* skip */
  }
  else {
    printf("[MPICPL] parser warning: unknown tag \"%s\" ignored!\n", elname);
    /* skip */
  }
  
  
  /* parse children recursively */
  xmlNodePtr child_node = current_node->children;
  
  while(child_node != NULL) {
    parse(cpl, child_node, argc, xmlargn, xmlargv);
    child_node = child_node->next;    
  }
  
}

/* ***************************************** */

static char ** parseArgs(xmlDocPtr xmldoc, char** xmlargv, int * pargc)
{
  int argc = 0;
  while(xmlargv[argc]) argc++;

  xmlNodePtr node = xmldoc->children;
  while(node != NULL) {
    char* elname = (char*)node->name;
    if(strcasecmp(elname, "universe") == 0) break;
    node = node->next;    
  }
  assert(node != NULL);

  char * args = (char*)xmlGetProp(node, (xmlChar*)"args");
  char * argz;
  size_t argz_len; /* full string length */
  argz_create_sep(args, ',', &argz, &argz_len);
  assert(argc == argz_count(argz, argz_len)); /* nb of substrings */
  char ** xmlargn = malloc((argc+1)*sizeof(char*));
  argz_extract(argz, argz_len, xmlargn);
  
  /* out */
  *pargc = argc;
  return xmlargn;
}

/* ***************************************** */

int MPICPL_Load(MPICPL cpl,
		char* xmlfile,
		char* xmlargv[],
		char* dtdfile)
{
  MPICPL_PRINT_LOW(cpl, "LOAD %s (DTD %s)\n", xmlfile, dtdfile);

  /* Load XML Document */
  xmlDocPtr xmldoc = xmlParseFile(xmlfile);
  if(!xmldoc) return MPICPL_ERROR;

  /* Validate the XML against the DTD */  
  if(dtdfile) {
    xmlDtdPtr xmldtd = xmlParseDTD(NULL,dtdfile);  
    if(!xmldtd) return MPICPL_ERROR;
    xmldtd->name = xmlCharStrdup("universe"); /* root */
    xmldtd->parent = xmldoc;
    xmldtd->doc = xmldoc;
    xmldoc->intSubset = xmldtd;
    xmlValidCtxt * ctxt = malloc(sizeof(xmlValidCtxt));
    ctxt->userData = (void *)stderr;
    ctxt->error = (xmlValidityErrorFunc)fprintf;
    ctxt->warning = (xmlValidityWarningFunc)fprintf;
    if(!xmlValidateDocument(ctxt, xmldoc)) return MPICPL_ERROR;
    if(!xmlValidateDocumentFinal(ctxt, xmldoc)) return MPICPL_ERROR;
    free(ctxt);    
  }

  /* xml args */
  MPICPL_PRINT_HIGH(cpl, "PARSE XML ARGS\n");
  int argc = 0; 
  char ** xmlargn = parseArgs(xmldoc, xmlargv, &argc);
  MPICPL_PRINT_HIGH(cpl, "argc = %d\n", argc);
  for(int i = 0 ; i < argc ; i++)
    MPICPL_PRINT_HIGH(cpl, "[%d] %s = %s\n", i, xmlargn[i], xmlargv[i]);
  
  /* parsing */
  xmlNodePtr child_node = xmldoc->children;
  while(child_node != NULL) {
    parse(cpl, child_node, argc, xmlargn, xmlargv);
    child_node = child_node->next;    
  }
  
  return MPICPL_SUCCESS;
}


/* ***************************************** */
