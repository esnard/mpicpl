/* MPI Coupling Library (XML Launcher)      */
/* Filename: mpicplrun.c                    */
/* Author(s): A. Esnard                     */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <getopt.h>

#include "mpicplb.h"

const char * boot = "MPICPLRUN";
int prank, psize;

/* ***************************************** */

void usage(int argc, char* argv[]) 
{
  printf("usage: %s %s\n", argv[0],  
	 "[-d] [-k] [-v dtdfile] xmlfile [xmlargs ...]");
  printf("\t\t -v dtdfile: validate xmlfile against dtdfile\n");
  printf("\t\t -k: detached mode (kill mpicplrun after spawning codes)\n");
  printf("\t\t -d: debug mode\n");
  exit(EXIT_FAILURE);  
}

/* ***************************************** */

int main(int argc, char **argv)
{
  // input args

  int opt;
  int debug = MPICPL_DEBUG_NO;
  int detached = 0;
  char * dtdfile = NULL;
  
  while ((opt = getopt (argc, argv, "dkv:")) != -1) {
    switch (opt) {
      
    case 'd':
      debug = MPICPL_DEBUG_HIGH;
      break;

    case 'k':
      detached = 1;
      break;
      
    case 'v':
      dtdfile = optarg;
      break;
      
    default:
      usage(argc,argv);
    }
  }
  
  if(argc - optind < 1) usage(argc,argv); /* one compulsory arg */

  char * xmlfile = argv[optind]; 
  char ** xmlargv = argv + optind + 1;  
  
  // start the MPI framework
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  
  // start the MPICPL framework
  MPICPL cpl; 
  MPICPL_Init_boot(&cpl, MPI_COMM_WORLD, boot, detached, debug);

  // load XML desc
  int err = MPICPL_Load(cpl, xmlfile, xmlargv, dtdfile);
  if(err != MPICPL_SUCCESS)  MPI_Abort(MPI_COMM_WORLD, 1);
  
  // start the coupled codes
  MPICPL_Run_boot(cpl);

    // wait for all spawn codes to finish (and process requests)
  int req;  
  char msg[MPICPL_MAX_MSG_SIZE];
  
  if(!cpl->detached) {
    do {
      MPICPL_Wait(cpl, &req, msg);
      
      if(req ==  MPICPL_REQUEST_SYNCHRONIZE) {
	printf("[%s:%d] SYNCHRO\n", boot, prank);
      }
      else if(req ==  MPICPL_REQUEST_MESSAGE) {
	printf("[%s:%d] MESSAGE %s\n", boot, prank, msg);
      }
    }
    while(req != MPICPL_REQUEST_END);
  }
  
  // shutdown the framework
  MPICPL_Finalize_boot(cpl);
  
  // shutdown the MPI framework
  MPI_Finalize();
  printf("[%s:%d] Goodbye!\n", boot, prank);  
  
  return EXIT_SUCCESS;
}

/* ***************************************** */
