/* MPI Coupling: Spawn One Code              */
/* Filename: spawnone.c                      */
/* Author: aurelien.esnard@labri.fr          */

#include "mpi.h"
#include "mpicplb.h"
#include <stdio.h>
#include <stdlib.h>

int prank, psize;
MPICPL cpl;

/* ***************************************** */

int main( int argc, char *argv[] )
{
  
  if(argc < 4) {
    printf("Usage: %s id np code \"args...\"\n", argv[0]);
    return EXIT_FAILURE;
  }

  char * id = argv[1];
  int np = atoi(argv[2]);
  char * code = argv[3];
  char * args = argv[4];

  const char * boot = "boot";
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  printf("[%s:%d] START\n", boot, prank+1);
  
  MPICPL_Init_boot(&cpl, MPI_COMM_WORLD, boot, 0, MPICPL_DEBUG_HIGH);

  MPICPL_Add_code(cpl, id, code, NULL, np, args);
  
  if(prank == 0) MPICPL_Print(cpl);
  
  MPICPL_Run_boot(cpl); 
  
  int req;
  char msg[MPICPL_MAX_MSG_SIZE];
  
  do {
    MPICPL_Wait(cpl, &req, msg);  
    if(req ==  MPICPL_REQUEST_SYNCHRONIZE) {
      printf("[%s:%d] SYNCHRO\n", boot, prank+1);
    }
    else if(req ==  MPICPL_REQUEST_MESSAGE) {
      printf("[%s:%d] MESSAGE %s\n", boot, prank+1, msg);
    }
  }
  while(req != MPICPL_REQUEST_END);
  
  MPICPL_Finalize_boot(cpl);
  printf("[%s:%d] STOP\n", boot, prank+1);
  fflush(stdout);
  MPI_Finalize();
  return 0;
}

/* ***************************************** */
